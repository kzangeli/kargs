// -----------------------------------------------------------------------------
//
// FILE                  kargsParse.c - initialize command line argument parsing
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <stdlib.h>                    // calloc

#include "kbase/kBasicLog.h"           // KBL_V
#include "kbase/kTypes.h"              // KBool, KTRUE, KFALSE
#include "kbase/kMacros.h"             // K_VEC_SIZE

#include "kargs/KArgsStatus.h"         // KArgsStatus
#include "kargs/KArgsError.h"          // KARGS_ERROR_PUSH
#include "kargs/KArg.h"                // KArg
#include "kargs/kargsGlobals.h"        // kargsProgName
#include "kargs/kargsBuiltins.h"       // kargsBuiltins
#include "kargs/kargsConfig.h"         // Config vars
#include "kargs/kargInfoPopulate.h"    // kargInfoPopulate
#include "kargs/KArgsError.h"          // KARGS_ERROR_PUSH
#include "kargs/kargsInit.h"           // Own interface



// -----------------------------------------------------------------------------
//
// Definitions for min/max values of builtin C integer types
//
#define CHAR_MIN        (char) 0x80
#define CHAR_MAX        (char) 0x7F
#define UCHAR_MIN       (unsigned char) 0
#define UCHAR_MAX       (unsigned char) 0xFF
#define SHORT_MIN       (short) 0x8000
#define SHORT_MAX       (short) 0x7FFF
#define USHORT_MIN      (unsigned short) 0
#define USHORT_MAX      (unsigned short) 0xFFFF
#define INT_MIN         (int) 0x80000000
#define INT_MAX         (int) 0x7FFFFFFF
#define UINT_MIN        (unsigned int) 0
#define UINT_MAX        (unsigned int) 0xFFFFFFFF
#define LONG_MIN        (long long) 0x8000000000000000
#define LONG_MAX        (long long) 0x7FFFFFFFFFFFFFFF
#define ULONG_MIN       (unsigned long long) 0x0000000000000000
#define ULONG_MAX       (unsigned long long) 0xFFFFFFFFFFFFFFFF



// -----------------------------------------------------------------------------
//
// kargInfoV -
//
KArgInfo* kargInfoV;



// -----------------------------------------------------------------------------
//
// outOfBound - 
//
static KBool outOfBound
(
  KargType            type,
  long long           sVal,
  unsigned long long  uVal,
  char**              errorString
)
{
  switch (type)
  {
  case KaBool:
  case KaFloat:
  case KaString:
  case KaEnd:
    return KFALSE;

  case KaChar:
    if (sVal < CHAR_MIN)
    {
      *errorString = "lower than minimum value for type";
      return KTRUE;
    }

    if (sVal > CHAR_MAX)
    {
      *errorString = "higher than maximum value for type";
      return KTRUE;
    }
    break;

  case KaShort:
    if (sVal < SHORT_MIN)
    {
      *errorString = "lower than minimum value for type";
      return KTRUE;
    }

    if (sVal > SHORT_MAX)
    {
      *errorString = "higher than maximum value for type";
      return KTRUE;
    }
    break;

  case KaInt:
    KBL_V(("sVal:    %lld", sVal));
    KBL_V(("INT_MIN: %d", INT_MIN));
    KBL_V(("INT_MAX: %d", INT_MAX));

    if (sVal < INT_MIN)
    {
      *errorString = "lower than minimum value for type";
      return KTRUE;
    }

    if (sVal > INT_MAX)
    {
      *errorString = "higher than maximum value for type";
      return KTRUE;
    }
    break;

  case KaUChar:
    if (sVal < 0)
    {
      *errorString = "lower than minimum value for type";
      return KTRUE;
    }

    if (uVal > UCHAR_MAX)
    {
      *errorString = "higher than maximum value for type";
      return KTRUE;
    }
    break;

  case KaUShort:
    if (sVal < 0)
    {
      *errorString = "lower than minimum value for type";
      return KTRUE;
    }
    
    if (uVal > USHORT_MAX)
    {
      *errorString = "higher than maximum value for type";
      return KTRUE;
    }
    break;

  case KaUInt:
    if (sVal < 0)
    {
      *errorString = "lower than minimum value for type";
      return KTRUE;
    }
    
    if (uVal > UINT_MAX)
    {
      *errorString = "higher than maximum value for type";
      return KTRUE;
    }
    break;

  case KaULong:
  case KaLong:
    break;
  }

  return KFALSE;  // Meaning: NOT out-of-bounds (== OK)
}



// -----------------------------------------------------------------------------
//
// kargVectorCheck - check that the user input vector is OK
//
// 1.  No longName nor shortName
// 2.  User option with the same longName/shortName as a builtin (4 combinations)?
// 3.  User option with the same longName/shortName as some other user option (4 combinations)?
// 4.  Default value and range limits within min/max for its type?
// 5.  Default value within its max/min range?
// 6.  Range OK? (min <= max)
// 7.  Invalid KargType (compiler should warn - typecasts get thru that)
// 8.  NULL valueP
// 9.  Invalid KargSort (compiler should warn - typecasts get thru that)
// 10. NULL description
//
static KArgsStatus kargVectorCheck(KArg* kiV)
{
  int kiIx = 0;

  KBL_V(("In kargVectorCheck"));
  
  //
  // 1. Any user option with neither longName nor shortName?
  //
  KBL_V(("kargVectorCheck: Any user option with neither longName nor shortName?"));
  while (kiV[kiIx].type != KaEnd)
  {
    if ((kiV[kiIx].longName == NULL) || (kiV[kiIx].longName[0] == 0))
    {
      if ((kiV[kiIx].shortName == NULL) || (kiV[kiIx].shortName[0] == 0))
      {
        KARGS_ERROR_PUSH("No Name", KasBadParam, "Option without name");
        return KasBadParam;
      }
    }

    ++kiIx;
  }


  //
  // 2. Any user option with the same longName/shortName as a builtin?
  //
  KBL_V(("kargVectorCheck: Any user option with the same longName/shortName as a builtin?"));
  kiIx = 0;
  while (kiV[kiIx].type != KaEnd)
  {
    int bIx = 0;

    while (kargsBuiltins[bIx].type != KaEnd)
    {
      if (kiV[kiIx].longName != NULL)
      {
        if (strcmp(kiV[kiIx].longName, kargsBuiltins[bIx].longName) == 0)
        {
          KARGS_ERROR_PUSH(kiV[kiIx].longName, KasNameTaken, "Option long-name already in use (by builtin long-name)");
          return KasNameTaken;
        }
        
        if (strcmp(kiV[kiIx].longName, kargsBuiltins[bIx].shortName) == 0)
        {
          KARGS_ERROR_PUSH(kiV[kiIx].longName, KasNameTaken, "Option long-name already in use (by builtin short-name)");
          return KasNameTaken;
        }
      }

      if (kiV[kiIx].shortName != NULL)
      {
        if (strcmp(kiV[kiIx].shortName, kargsBuiltins[bIx].longName) == 0)
        {
          KARGS_ERROR_PUSH(kiV[kiIx].shortName, KasNameTaken, "Option short-name already in use (by builtin long-name)");
          return KasNameTaken;
        }

        if (strcmp(kiV[kiIx].shortName, kargsBuiltins[bIx].shortName) == 0)
        {
          KARGS_ERROR_PUSH(kiV[kiIx].shortName, KasNameTaken, "Option short-name already in use (by builtin short-name)");
          return KasNameTaken;
        }
      }

      ++bIx;
    }

    ++kiIx;
  }


  //
  // 3. Any user option with the same longName/shortName as some other user option?
  //
  KBL_V(("kargVectorCheck: Any user option with the same longName/shortName some other user option?"));
  int ix1 = 0;
  while (kiV[ix1].type != KaEnd)
  {
    int ix2 = 0;

    while (kiV[ix2].type != KaEnd)
    {
      if (ix1 == ix2)  // Will not compare an option to itself
      {
        ++ix2;
        continue;
      }
      
      if (kiV[ix1].longName != NULL)
      {
        KBL_V(("Testing longName '%s'", kiV[ix1].longName));
        if (kiV[ix2].longName != NULL)
        {
          KBL_V(("Checking against longName '%s'", kiV[ix2].longName));
          if (strcmp(kiV[ix1].longName, kiV[ix2].longName) == 0)
          {
            KARGS_ERROR_PUSH(kiV[kiIx].longName, KasNameTaken, "Option long-name already in use (by other options long-name)");
            return KasNameTaken;
          }
        }

        if (kiV[ix2].shortName != NULL)
        {
          KBL_V(("Checking against shortName '%s'", kiV[ix2].shortName));
          if (strcmp(kiV[ix1].longName, kiV[ix2].shortName) == 0)
          {
            KARGS_ERROR_PUSH(kiV[kiIx].longName, KasNameTaken, "Option long-name already in use (by other options short-name)");
            return KasNameTaken;
          }
        }
      }

      if (kiV[ix1].shortName != NULL)
      {
        KBL_V(("Testing shortName '%s'", kiV[ix1].shortName));
        if (kiV[ix2].longName != NULL)
        {
          KBL_V(("Checking against longName '%s'", kiV[ix2].longName));
          if (strcmp(kiV[ix1].shortName, kiV[ix2].longName) == 0)
          {
            KARGS_ERROR_PUSH(kiV[kiIx].shortName, KasNameTaken, "Option short-name already in use (by other options short-name)");
            return KasNameTaken;
          }
        }

        if (kiV[ix2].shortName != NULL)
        {
          KBL_V(("Checking against shortName '%s'", kiV[ix2].shortName));
          if (strcmp(kiV[ix1].shortName, kiV[ix2].shortName) == 0)
          {
            KARGS_ERROR_PUSH(kiV[kiIx].shortName, KasNameTaken, "Option short-name already in use (by other options short-name)");
            return KasNameTaken;
          }
        }
      }

      ++ix2;
    }

    ++ix1;
  }


  //
  // 4. Default value and range limits within min/max for its type?
  //    E.g. a max-value of 256 for a KaUChar is an error as an 8-bit value is 0-255
  //
  KBL_V(("kargVectorCheck: Default+Range-Limits within min/max for type?"));
  int ix = 0;
  while (kiV[ix].type != KaEnd)
  {
    char*               name   = (kiV[ix].longName == NULL)? (char*) kiV[ix].shortName : (char*) kiV[ix].longName;
    long long           sMin   = (long long) kiV[ix].min;
    unsigned long long  uMin   = (unsigned long long) kiV[ix].min;
    long long           sMax   = (long long) kiV[ix].max;
    unsigned long long  uMax   = (unsigned long long) kiV[ix].max;
    long long           sDef   = (long long) kiV[ix].def;
    unsigned long long  uDef   = (unsigned long long) kiV[ix].def;
    char*               errorString;

    // Min limit out of bounds?
    if (sMin != KA_NL_NUMBER)
    {
      KBL_V(("sMin:      %lld (0x%llx)", sMin, sMin));
      KBL_V(("LONG_MIN:  %lld (0x%llx)", LONG_MIN, LONG_MIN));
      KBL_V(("uMin:      %llu (0x%llx)", uMin, uMin));
      KBL_V(("ULONG_MIN: %llu (0x%llx)", ULONG_MIN, ULONG_MIN));
      if (outOfBound(kiV[ix].type, sMin, uMin, &errorString) == KTRUE)
      {
        KARGS_ERROR_PUSH(name, KasBadParam, errorString);
        return KasBadParam;
      }
    }

    // Max limit out of bounds?
    if (sMax != KA_NL_NUMBER)
    {
      if (outOfBound(kiV[ix].type, sMax, uMax, &errorString) == KTRUE)
      {
        KARGS_ERROR_PUSH(name, KasBadParam, errorString);
        return KasBadParam;
      }
    }

    // Default value out of bounds?
    if (sDef != KA_NL_NUMBER)
    {
      if (outOfBound(kiV[ix].type, sDef, uDef, &errorString) == KTRUE)
      {
        KARGS_ERROR_PUSH(name, KasBadParam, errorString);
        return KasBadParam;
      }
    }

    ++ix;
  }        


  //
  // 5. Default value within its range?
  //
  KBL_V(("kargVectorCheck: Default value within its range?"));
  while (kiV[ix].type != KaEnd)
  {
    char*               name   = (kiV[ix].longName == NULL)? (char*) kiV[ix].shortName : (char*) kiV[ix].longName;
    long long           sMin   = (long long) kiV[ix].min;
    unsigned long long  uMin   = (unsigned long long) kiV[ix].min;
    long long           sMax   = (long long) kiV[ix].max;
    unsigned long long  uMax   = (unsigned long long) kiV[ix].max;
    long long           sDef   = (long long) kiV[ix].def;
    unsigned long long  uDef   = (unsigned long long) kiV[ix].def;
    
    switch (kiV[ix].type)
    {
    case KaBool:
    case KaFloat:
    case KaString:
    case KaEnd:
      break;

    case KaChar:
    case KaShort:
    case KaInt:
    case KaLong:
      if ((sMin != KA_NL_NUMBER) && (sDef < sMin))
      {
        KARGS_ERROR_PUSH(name, KasBadParam, "Default value lower than the minimum limit for option");
        return KasBadParam;
      }

      if ((sMax != KA_NL_NUMBER) && (sDef > sMax))
      {
        KARGS_ERROR_PUSH(name, KasBadParam, "Defaultvalue exceeds the maximum limit for option");
        return KasBadParam;
      }
      break;

    case KaUChar:
    case KaUShort:
    case KaUInt:
    case KaULong:
      if ((uMin != KA_NL_NUMBER) && (uDef < uMin))
      {
        KARGS_ERROR_PUSH(name, KasBadParam, "Default value lower than the minimum limit for option");
        return KasBadParam;
      }
      if ((uMax != KA_NL_NUMBER) && (uDef > uMax))
      {
        KARGS_ERROR_PUSH(name, KasBadParam, "Default value exceeds the maximum limit for option");
        return KasBadParam;
      }
      break;
    }

    ++ix;
  }


  //
  // 6. Min Limit > Max Limit?
  //    Def <= Max Limit?
  //    Def >= Min Limit?
  //
  KBL_V(("kargVectorCheck: Min > Max?"));
  ix = 0;
  while (kiV[ix].type != KaEnd)
  {
    char*               name   = (kiV[ix].longName == NULL)? (char*) kiV[ix].shortName : (char*) kiV[ix].longName;
    long long           sMin   = (long long) kiV[ix].min;
    unsigned long long  uMin   = (unsigned long long) kiV[ix].min;
    long long           sMax   = (long long) kiV[ix].max;
    unsigned long long  uMax   = (unsigned long long) kiV[ix].max;
    long long           sDef   = (long long) kiV[ix].def;
    unsigned long long  uDef   = (unsigned long long) kiV[ix].def;

    // If any part is unlimited, skip the test
    if ((sMin == KA_NL_NUMBER) || (sMax == KA_NL_NUMBER))
    {
      ++ix;
      continue;
    }

    switch (kiV[ix].type)
    {
    case KaBool:
    case KaFloat:
    case KaString:
    case KaEnd:
      break;

    case KaChar:
    case KaShort:
    case KaInt:
    case KaLong:
      if (sMin > sMax)
      {
        KARGS_ERROR_PUSH(name, KasBadParam, "Minimum limit is higher than the maximum limit for option");
        return KasBadParam;
      }

      if (sDef > sMax)
      {
        KARGS_ERROR_PUSH(name, KasBadParam, "Default value is higher than the maximum limit for option");
        return KasBadParam;
      }

      if (sDef < sMin)
      {
        KARGS_ERROR_PUSH(name, KasBadParam, "Default value is lower than the minimum limit for option");
        return KasBadParam;
      }
      break;

    case KaUChar:
    case KaUShort:
    case KaUInt:
    case KaULong:
      if (uMin > uMax)
      {
        KARGS_ERROR_PUSH(name, KasBadParam, "Minimum limit is higher than the maximum limit for option");
        return KasBadParam;
      }

      if (uDef > uMax)
      {
        KARGS_ERROR_PUSH(name, KasBadParam, "Default value is higher than the maximum limit for option");
        return KasBadParam;
      }

      if (uDef < uMin)
      {
        KARGS_ERROR_PUSH(name, KasBadParam, "Default value is lower than the minimum limit for option");
        return KasBadParam;
      }
    }
    ++ix;
  }


  //
  // 7. Invalid KargType (compiler should warn - typecasts get thru that)
  //
  KBL_V(("kargVectorCheck: Invalid KargType?"));
  ix = 0;
  while (kiV[ix].type != KaEnd)
  {
    char* name   = (kiV[ix].longName == NULL)? (char*) kiV[ix].shortName : (char*) kiV[ix].longName;

    if ((kiV[ix].type < KaBool) || (kiV[ix].type > KaEnd))
    {
      KARGS_ERROR_PUSH(name, KasBadParam, "Invalid 'type' for option");
      return KasBadParam;
    }
    
    ++ix;
  }


  //
  // 8. NULL valueP
  //
  KBL_V(("kargVectorCheck: NULL valueP?"));
  ix = 0;
  while (kiV[ix].type != KaEnd)
  {
    char* name   = (kiV[ix].longName == NULL)? (char*) kiV[ix].shortName : (char*) kiV[ix].longName;

    if (kiV[ix].valueP == NULL)
    {
      KARGS_ERROR_PUSH(name, KasBadParam, "NULL value pointer for option");
      return KasBadParam;
    }
    
    ++ix;
  }


  //
  // 9. Invalid KargSort (compiler should warn - typecasts get thru that)
  //
  KBL_V(("kargVectorCheck: Invalid KargSort?"));
  ix = 0;
  while (kiV[ix].type != KaEnd)
  {
    char* name   = (kiV[ix].longName == NULL)? (char*) kiV[ix].shortName : (char*) kiV[ix].longName;

    if ((kiV[ix].sort < KaOpt) || (kiV[ix].sort > KaHid))
    {
      KARGS_ERROR_PUSH(name, KasBadParam, "Invalid 'sort' for option");
      return KasBadParam;
    }

    ++ix;
  }


  //
  // 10. NULL description
  //
  KBL_V(("kargVectorCheck: NULL description?"));
  ix = 0;
  while (kiV[ix].type != KaEnd)
  {
    char* name   = (kiV[ix].longName == NULL)? (char*) kiV[ix].shortName : (char*) kiV[ix].longName;

    if (kiV[ix].description == NULL)
    {
      KARGS_ERROR_PUSH(name, KasBadParam, "NULL description pointer for option");
      return KasBadParam;
    }

    ++ix;
  }

  return KasOk;
}



// -----------------------------------------------------------------------------
//
// defaultValues -
//
static KArgsStatus defaultValues(KArgInfo* kiV)
{
  int kargIx = 0;

  while (kiV[kargIx].type != KaEnd)
  {
    KArgInfo* kargP = &kiV[kargIx];

    switch (kargP->type)
    {
    case KaBool:
      *((KBool*) kargP->valueP) = (KBool) (long long) kargP->def;
      break;

    case KaString:
      *((char**) kargP->valueP) = (char*) kargP->def;
      break;

    case KaFloat:
      *((float*) kargP->valueP) = (float) (long long) kargP->def;
      break;

    case KaInt8:
      *((char*) kargP->valueP) = (char) (long long) kargP->def;
      break;

    case KaUInt8:
      *((unsigned char*) kargP->valueP) = (unsigned char) (long long) kargP->def;
      break;

    case KaInt16:
      *((short*) kargP->valueP) = (short) (long long) kargP->def;
      break;

    case KaUInt16:
      *((unsigned short*) kargP->valueP) = (unsigned short) (long long) kargP->def;
      break;

    case KaInt32:
      *((int*) kargP->valueP) = (int) (long long) kargP->def;
      break;

    case KaUInt32:
      *((unsigned int*) kargP->valueP) = (unsigned int) (long long) kargP->def;
      break;

    case KaInt64:
      *((long long*) kargP->valueP) = (long long) kargP->def;
      break;

    case KaUInt64:
      *((unsigned long long*) kargP->valueP) = (unsigned long long) kargP->def;
      break;

    case KaEnd:
      break;
    }

    ++kargIx;
  }

  return KasOk;
}



// -----------------------------------------------------------------------------
//
// envVarValues -
//
static KArgsStatus envVarValues(KArgInfo* kiV)
{
  for (int kargIx = 0; kiV[kargIx].type != KaEnd; kargIx++)
  {
    KArgInfo* kargP       = &kiV[kargIx];
    char*     envVarValue = getenv(kargP->envVar);
    KBool     bValue      = KFALSE;

    if (envVarValue == NULL)
      continue;

    unsigned long long iValue = atoi(envVarValue);

    if (kargP->type == KaBool)
    {
      if (strcasecmp(envVarValue, "TRUE") == 0)
        bValue = KTRUE;
    }

    switch (kargP->type)
    {
    case KaBool:      *((KBool*)              kargP->valueP) = bValue;                              break;
    case KaString:    *((char**)              kargP->valueP) = (char*) envVarValue;                 break;
    case KaFloat:     *((float*)              kargP->valueP) = (float) strtod(envVarValue, NULL);   break;
    case KaInt8:      *((char*)               kargP->valueP) = (char)  iValue;                      break;
    case KaUInt8:     *((unsigned char*)      kargP->valueP) = (unsigned char)  iValue;             break;
    case KaInt16:     *((short*)              kargP->valueP) = (short) iValue;                      break;
    case KaUInt16:    *((unsigned short*)     kargP->valueP) = (unsigned short) iValue;             break;
    case KaInt32:     *((int*)                kargP->valueP) = (int)   iValue;                      break;
    case KaUInt32:    *((unsigned int*)       kargP->valueP) = (unsigned int)   iValue;             break;
    case KaInt64:     *((long long*)          kargP->valueP) = (long long)      iValue;             break;
    case KaUInt64:    *((unsigned long long*) kargP->valueP) = (unsigned long long) iValue;         break;
    case KaEnd:
      break;
    }

    kargP->from = KavfEnvVar;
  }

  return KasOk;
}



// -----------------------------------------------------------------------------
//
// kargsInit -
//
KArgsStatus kargsInit(const char* progName, KArg* kargV, const char* prefix)
{
  KArgsStatus ks;

  if (kargV == NULL)
  {
    KARGS_ERROR_PUSH("global", KasBadParam, "NULL argument vector");
    return KasBadParam;
  }
  
  kargsProgName = (char*) progName;
  kargsPrefix   = (char*) prefix;

  //
  // Check integrity of the KArg vector.
  // Make sure:
  //   o all min values are lower than the max values,
  //   o the default values are in between limits,
  //   o what more?
  //
  KBL_V(("Calling kargVectorCheck"));
  ks = kargVectorCheck(kargV);
  if (ks != KasOk)
  {
    KARGS_ERROR_PUSH("global", ks, "kargVectorCheck error");
    return ks;
  }
  

  //
  // Create the KArgInfo vector from the user defined KArg vector and the builtins
  //
  int userOptions = 0;
  int builtins    = 0;
  int options     = 0;

  while (kargsBuiltins[builtins].type != KaEnd)
    ++builtins;
  KBL_V(("%d builtins", builtins));

  while (kargV[userOptions].type != KaEnd)
    ++userOptions;
  KBL_V(("%d user defined options", userOptions));

  options = builtins + userOptions + 1;

  KBL_V(("Allocating room for %d options", options));
  kargInfoV = (KArgInfo*) calloc(options, sizeof(KArgInfo));

  if (kargInfoV == NULL)
  {
    KARGS_ERROR_PUSH("global", KasOutOfMemory, "allocating KArgInfo array");
    return KasOutOfMemory;
  }
  
  int ix = 0;

  KBL_V(("Calling kargInfoPopulate for builtins"));
  KBL_V(("Calling kargInfoPopulate for user defined options"));
  kargInfoPopulate(kargInfoV, kargsBuiltins, KTRUE,  &ix);
  kargInfoPopulate(kargInfoV, kargV,         KFALSE, &ix);

  // Mark the last slot as END
  kargInfoV[ix].type = KaEnd;


  //
  // Set default values for all option variables
  //
  ks = defaultValues(kargInfoV);
  if (ks != KasOk)
  {
    KARGS_ERROR_PUSH("global", ks, "error in default values");
    return ks;
  }

  //
  // Set values for env vars
  //
  ks = envVarValues(kargInfoV);
  if (ks != KasOk)
  {
    KARGS_ERROR_PUSH("global", ks, "error in env values");
    return ks;
  }
  
  return KasOk;
}

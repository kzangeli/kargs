# 
# FILE            makefile
# 
# AUTHOR          Ken Zangelin
# 
# Copyright 2024 Ken Zangelin
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with theLicense.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
LIB_SO        = libkargs.so
LIB           = libkargs.a
CC            = gcc
INCLUDE       = -I..
DFLAGS        = -DANSI -DKBLOG_ON
CFLAGS        = -g -Wall -fPIC -Wno-unused-function $(DFLAGS) $(INCLUDE)
LIB_SOURCES   = kargsConfig.c       \
                kargsGlobals.c      \
                kargsParse.c        \
                kargsUsage.c        \
                kargsPeek.c         \
                kargInfoPopulate.c  \
                kargsBuiltins.c     \
                kargsInit.c         \
                KArgsError.c        \
                kargsStatus.c
LIB_OBJS      = $(LIB_SOURCES:c=o)
TEST          = kargsTest
TEST_SOURCES  = kargsTest.c
TEST_OBJS     = $(TEST_SOURCES:c=o)
LIBS          = ../kbase/libkbase.a

all: $(LIB_SO) $(LIB) $(TEST) $(HTOA)

clean:
						rm -f $(LIB_OBJS)
						rm -f $(LIB_SO)
						rm -f $(LIB)
						rm -f $(TEST) $(TEST_OBJS)

i:          install

install:    all
						@if [ ! -d bin ]; then mkdir bin; fi
						cp $(TEST) bin/

di:         install

ci:         clean install

$(LIB):			$(LIB_OBJS) $(LIB_SOURCES)
						ar r $(LIB) $(LIB_OBJS)
						ranlib $(LIB)

$(LIB_SO):	$(LIB_OBJS) $(LIB_SOURCES)
						$(CC) -shared $(LIB_OBJS) -o $(LIB_SO)

$(TEST):		$(TEST_OBJS) $(LIB)
						$(CC) -o $(TEST) $(TEST_OBJS) $(LIB)  $(LIBS)


%.o: %.c
						$(CC) $(CFLAGS) -c $^ -o $@

%.i: %.c
						$(CC) $(CFLAGS) -c $^ -E > $@

%.cs: %.c
						$(CC) -Wa,-adhln -g $(CFLAGS)  $^  > $@

#ifndef KARGS_KARGSCONFIG_H_
#define KARGS_KARGSCONFIG_H_

// -----------------------------------------------------------------------------
//
// FILE                  kargsConfig.h - configure kargs library
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include "kbase/kTypes.h"              // KBool
#include "kargs/KArgsStatus.h"         // KArgsStatus



// -----------------------------------------------------------------------------
//
// kargsConfig -
//
typedef enum KArgsConfigItem
{
  KargsPrefix,
  KargsExitOnUsage,
  KargsExitOnUsageExitCode
} KArgsConfigItem;




// -----------------------------------------------------------------------------
//
// Config vars
//
extern char* kargsPrefix;
extern KBool kargsExitOnUsage;
extern int   kargsExitOnUsageExitCode;



// -----------------------------------------------------------------------------
//
// kargsConfig -
//
extern KArgsStatus kargsConfig(KArgsConfigItem cItem, char* value);

#endif  // KARGS_KARGSCONFIG_H_

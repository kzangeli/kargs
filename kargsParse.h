#ifndef KARGS_KARGSPARSE_H_
#define KARGS_KARGSPARSE_H_

// -----------------------------------------------------------------------------
//
// FILE                  kargsParse.h - heart of kargs library
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include "kargs/KArgsStatus.h"         // KArgsStatus
#include "kargs/KArg.h"                // KArg



// -----------------------------------------------------------------------------
//
// kargsParse -
//
extern KArgsStatus kargsParse(int argC, char* argV[]);

#endif  // KARGS_KARGSPARSE_H_

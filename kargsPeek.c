// -----------------------------------------------------------------------------
//
// FILE                  kargsPeek.c - peek inside command line arguments
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include "kbase/kBasicLog.h"           // KBL_*

#include "kargs/KArg.h"                // KArg
#include "kargs/KArgsError.h"          // KARGS_ERROR_PUSH
#include "kargs/kargsBuiltins.h"       // kargsBuiltins
#include "kargs/kargsPeek.h"           // Own interface



// -----------------------------------------------------------------------------
//
// kargsPeek - returns the string of the value of the option 'optLongName', if set
//
char* kargsPeek(int argC, char* argV[], KArg* kargV, char* optLongName)
{
  if ((argV == NULL) || (kargV == NULL))
  {
    KARGS_ERROR_PUSH(optLongName, KasBadParam, "NULL parameter");
    return NULL;
  }
  
  // 1. Lookup option in builtins
  KArg* optP   = NULL;
  int   kargIx = 0;

  while (kargV[kargIx].type != KaEnd)
  {
    if (strcmp(optLongName, kargV[kargIx].longName) == 0)
    {
      optP = &kargV[kargIx];
      break;
    }

    if (kargV[kargIx].shortName != NULL)
    {
      if (strcmp(optLongName, kargV[kargIx].shortName) == 0)
      {
        optP = &kargV[kargIx];
        break;
      }
    }

    ++kargIx;
  }

  // If not found, Lookup option in builtins
  if (optP == NULL)
  {
    kargIx = 0;
    while (kargsBuiltins[kargIx].type != KaEnd)
    {
      if (strcmp(optLongName, kargsBuiltins[kargIx].longName) == 0)
      {
        optP = &kargsBuiltins[kargIx];
        break;
      }
      
      if (kargsBuiltins[kargIx].shortName != NULL)
      {
        if (strcmp(optLongName, kargsBuiltins[kargIx].shortName) == 0)
        {
          optP = &kargsBuiltins[kargIx];
          break;
        }
      }

      ++kargIx;
    }
  }

  if (optP == NULL)
  {
    // Not really an error, just not found ...
    KARGS_ERROR_PUSH(optLongName, KasBadParam, "no such option");
    return NULL;
  }
  
  // 3. Find option in given arguments
  int ix = 0;

  //
  // FIXME P1: This lookup harbors a possible bug ...
  //           Imagine we have an option called --abc and the program is called with
  //           a value of any of the options with that exact string. E.g.:
  //             % exec -f --abc
  //           Being -f a string option getting the value --abc
  //
  //           This seems highly unlikely but nevertheless, could happen
  //           To fix this, a more intelligent lookup must be implemented, ignoring
  //           all values, only looking at options. I.e. consider '-f' but NOT its value '--abc'
  //
  while (ix < argC)
  {
    if (strcmp(optLongName, argV[ix]) == 0)
    {
      if (optP->type == KaBool)
        return "SET";
      else
      {
        if (ix == argC - 1)  // Error: no value for non-bool option
        {
          KARGS_ERROR_PUSH(optLongName, KasBadParam, "no value for non-bool option");
          return NULL;
        }

        return argV[ix + 1];
      }
    }

    ++ix;
  }

  return NULL;  // option 'optLongName' not found 
}

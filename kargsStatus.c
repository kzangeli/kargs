// -----------------------------------------------------------------------------
//
// FILE                  kargsStatus.c - convert KArgsStatus to English text
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include "kargs/KArgsStatus.h"          // Own interface



// -----------------------------------------------------------------------------
//
// kargsStatus - convert KArgsStatus to English text
//
char* kargsStatus(KArgsStatus status)
{
  switch (status)
  {
  case KasOk:                     return "ok";
  case KasBadParam:               return "bad parameter";
  case KasValueMissing:           return "value missing";
  case KasInvalidValue:           return "invalid value";
  case KasInvalidConfigItem:      return "invalid config Item";
  case KasOutOfMemory:            return "out of memory";
  case KasOutOfBounds:            return "out of bounds";
  case KasCharOutOfMinLimit:      return "char out of min limit";
  case KasShortOutOfMinLimit:     return "short out of min limit";
  case KasIntOutOfMinLimit:       return "int out of min limit";
  case KasLongOutOfMinLimit:      return "long out of min limit";
  case KasUCharOutOfMinLimit:     return "unsigned char out of min limit";
  case KasUShortOutOfMinLimit:    return "unsigned short out of min limit";
  case KasUIntOutOfMinLimit:      return "unsigned int out of min limit";
  case KasULongOutOfMinLimit:     return "unsigned long out of min limit";
  case KasFloatOutOfMinLimit:     return "float out of min limit";
  case KasStringOutOfMinLimit:    return "string out of min limit";
  case KasCharOutOfMaxLimit:      return "char out of max limit";
  case KasShortOutOfMaxLimit:     return "short out of max limit";
  case KasIntOutOfMaxLimit:       return "int out of max limit";
  case KasLongOutOfMaxLimit:      return "long out of max limit";
  case KasUCharOutOfMaxLimit:     return "unsigned char out of max limit";
  case KasUShortOutOfMaxLimit:    return "unsigned short out of max limit";
  case KasUIntOutOfMaxLimit:      return "unsigned int out of max limit";
  case KasULongOutOfMaxLimit:     return "unsigned long out of max limit";
  case KasFloatOutOfMaxLimit:     return "float out of max limit";
  case KasStringOutOfMaxLimit:    return "string out of max limit";    
  case KasNameTaken:              return "name taken";
  }

  return "Unknown Error";
}

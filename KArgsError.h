#ifndef KARGS_KARGSERROR_H_
#define KARGS_KARGSERROR_H_

// -----------------------------------------------------------------------------
//
// FILE                  KArgsError.h - KArgsError struct for error handling
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <errno.h>                      // errno

#include "kargs/KArgsStatus.h"          // KArgsStatus



// -----------------------------------------------------------------------------
//
// KARGS_ERROR_PUSH -
//
#define KARGS_ERROR_PUSH(optionName, status, description)                                       \
do                                                                                              \
{                                                                                               \
  KBL_E(("Kargs Error %s for option '%s': %s", kargsStatus(status), optionName, description));  \
  kargsErrorPush(__FILE__, __LINE__, __FUNCTION__, optionName, errno, status, description);     \
} while (0)



// -----------------------------------------------------------------------------
//
// KArgsError -
//
typedef struct KArgsError
{
  char*              fileName;
  int                lineNo;
  char*              funcName;
  char*              optionName;
  int                errNo;
  KArgsStatus        status;
  char*              description;
  struct KArgsError* next;
} KArgsError;



// -----------------------------------------------------------------------------
//
// kargsErrorPush - push an error to the error stack
//
// This is an internal function. It should be used *only* by the internal macro 'KARGS_ERROR_PUSH'.
//
extern void kargsErrorPush
(
  const char*  fileName,
  int          lineNo,
  const char*  funcName,
  const char*  optionName,
  int          errNo,
  KArgsStatus  status,
  const char*  description
);



// -----------------------------------------------------------------------------
//
// kargsErrorGet -
//
extern KArgsError* kargsErrorGet(void);

#endif  // KARGS_KARGSERROR_H_

#ifndef KARGS_KARGINFO_H_
#define KARGS_KARGINFO_H_

// -----------------------------------------------------------------------------
//
// FILE                  KArgInfo.h - KArgInfo struct to hold info on karg options
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include "kbase/kTypes.h"              // KBool

#include "kargs/KargType.h"            // KargType, KargValueFrom
#include "kargs/KargSort.h"            // KargSort



// -----------------------------------------------------------------------------
//
// KArgInfo -
//
typedef struct KArgInfo
{
  char*          longName;       // Long name of the option, e.g. --port
  char*          shortName;      // MUST start with '-' or '+'
  KargType       type;           // type: bool, int, string, etc
  void*          valueP;         // points to where to store the value
  KargSort       sort;           // sort: optional, required, hidden
  void*          def;            // default value
  void*          min;            // lower limit
  void*          max;            // upper limit
  char*          description;    // textual description for usage

  KBool          builtin;
  KBool          disabled;
  KargValueFrom  from;
  char           envVar[60];
} KArgInfo;



// -----------------------------------------------------------------------------
//
// KARGS_INFO_END - last item in KArgInfo vector
//
#define KARGS_INFO_END { "", "", KaEnd, NULL, KaReq, 0, 0, 0, NULL, KFALSE, KFALSE, KavfDefaultValue, { 0 } }

#endif  // KARGS_KARGINFO_H_

// -----------------------------------------------------------------------------
//
// FILE                  kargsConfig.h - configure kargs library
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <string.h>                    // strdup
#include <stdlib.h>                    // atoi

#include "kbase/kTypes.h"              // KBool
#include "kbase/kBasicLog.h"           // KBL_*

#include "kargs/KArgsStatus.h"         // KArgsStatus
#include "kargs/KArgsError.h"          // KARGS_ERROR_PUSH
#include "kargs/kargsConfig.h"         // Own interface



// -----------------------------------------------------------------------------
//
// Config vars
//
char* kargsPrefix              = "";
KBool kargsExitOnUsage         = KTRUE;
int   kargsExitOnUsageExitCode = 0;



// -----------------------------------------------------------------------------
//
// kargsConfig -
//
KArgsStatus kargsConfig(KArgsConfigItem cItem, char* value)
{
  switch (cItem)
  {
  case KargsPrefix:
    kargsPrefix = strdup(value);
    break;
    
  case KargsExitOnUsageExitCode:
    kargsExitOnUsageExitCode = atoi(value);
    break;

  case KargsExitOnUsage:
    if ((strcasecmp(value, "yes") == 0) || (strcasecmp(value, "on") == 0))
      kargsExitOnUsage = KTRUE;
    else if ((strcasecmp(value, "no") == 0) || (strcasecmp(value, "off") == 0))
      kargsExitOnUsage = KFALSE;
    else
    {
      KARGS_ERROR_PUSH("ExitOnUsage", KasInvalidConfigItem, value);
      return KasInvalidConfigItem;
    }
    break;
  }

  return KasOk;
}

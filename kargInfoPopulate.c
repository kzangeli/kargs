// -----------------------------------------------------------------------------
//
// FILE                  kargInfoPopulate.c - populate the KArgInfo vector
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include "kbase/kBasicLog.h"           // KBL_*
#include "kbase/kTypes.h"              // KBool, KTRUE, KFALSE

#include "kargs/KArgInfo.h"            // KArgInfo
#include "kargs/kargsConfig.h"         // kargsPrefix
#include "kargs/kargInfoPopulate.h"    // Own interface



// -----------------------------------------------------------------------------
//
// kargInfoPopulate - populate the KArgInfo vector
//
void kargInfoPopulate(KArgInfo* kargInfoV, KArg* kargV, KBool builtin, int* ixP)
{
  int ix   = 0;    // Index in kargV (input)
  int kiIx = *ixP; // Index in kargInfoV (output)

  while (kargV[ix].type != KaEnd)
  {
    kargInfoV[kiIx].builtin      = builtin;
    kargInfoV[kiIx].disabled     = KFALSE;
    kargInfoV[kiIx].from         = KavfDefaultValue;

    kargInfoV[kiIx].longName     = (char*) kargV[ix].longName;
    kargInfoV[kiIx].shortName    = (char*) kargV[ix].shortName;
    kargInfoV[kiIx].type         = kargV[ix].type;
    kargInfoV[kiIx].valueP       = kargV[ix].valueP;
    kargInfoV[kiIx].sort         = kargV[ix].sort;
    kargInfoV[kiIx].def          = kargV[ix].def;
    kargInfoV[kiIx].min          = kargV[ix].min;
    kargInfoV[kiIx].max          = kargV[ix].max;
    kargInfoV[kiIx].description  = (char*) kargV[ix].description;


    //
    // Build env var from longName and kargsPrefix
    //
    // Builtin --usage and --Usage have no env var
    //
    if ((kargInfoV[kiIx].longName != NULL) && (strcmp(kargInfoV[kiIx].longName, "--usage") == 0))
    {
      kargInfoV[kiIx].envVar[0] = 0;

      ++ix;
      ++kiIx;

      continue;
    }
    if ((kargInfoV[kiIx].longName != NULL) && (strcmp(kargInfoV[kiIx].longName, "--Usage") == 0))
    {
      kargInfoV[kiIx].envVar[0] = 0;

      ++ix;
      ++kiIx;

      continue;
    }
    
    int   prefixLen   = (kargsPrefix == NULL)? 0 : strlen(kargsPrefix);
    int   onIx        = 0;  // index of optName
    KBool lname       = (kargInfoV[kiIx].longName == NULL)? KFALSE : KTRUE;
    char* optName     = (lname == KTRUE)? (char*) kargInfoV[kiIx].longName  : (char*) kargInfoV[kiIx].shortName;

    if (kargsPrefix != NULL)
      sprintf(kargInfoV[kiIx].envVar, "%s", kargsPrefix);

    // If double '-' initially, skip one of the hyphens
    if ((optName[0] == '-') && (optName[1] == '-'))
      ++optName;

    while (prefixLen + onIx < sizeof(kargInfoV[kiIx].envVar))
    {
      char c = optName[onIx];

      if (c == 0)
        break;

      //
      // toupper + avoid forbidden chars.
      // Also, first char cannot be a digit if prefix is empty
      // All non [a-zA-Z0-0_] are transformed to underscores
      //
      if ((c >= 'A') && (c <= 'Z'))
        ;  // Keep as is
      else if ((c >= 'a') && (c <= 'z'))
        c = c - ('a' - 'A');
      else if ((onIx == 0) &&  (kargsPrefix[0] == 0) && ((c >= '0') && (c <= '9')))
        c = '_';
      else if (c == '_')
        ;  // OK
      else if (c == '-')
        c = '_';  // OK
      else if ((c >= '0') && (c <= '9'))
        ;  // OK
      else
        c = '_';

      kargInfoV[kiIx].envVar[prefixLen + onIx] = c;

      ++onIx;
    }

    kargInfoV[kiIx].envVar[prefixLen + onIx] = 0;
    KBL_V(("Env var for '%s': %s", kargInfoV[kiIx].longName, kargInfoV[kiIx].envVar));

    ++ix;
    ++kiIx;
  }

  *ixP = kiIx;
}

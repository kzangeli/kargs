#ifndef KARGS_KARGSSTATUS_H_
#define KARGS_KARGSSTATUS_H_

// -----------------------------------------------------------------------------
//
// FILE                  KArgsStatus.h - status code for kargs functions
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//



// -----------------------------------------------------------------------------
//
// KArgsStatus -
//
typedef enum KArgsStatus
{
  KasOk,
  KasBadParam,
  KasValueMissing,
  KasInvalidValue,
  KasInvalidConfigItem,
  KasOutOfMemory,
  KasOutOfBounds,
  KasCharOutOfMinLimit,
  KasShortOutOfMinLimit,
  KasIntOutOfMinLimit,
  KasLongOutOfMinLimit,
  KasUCharOutOfMinLimit,
  KasUShortOutOfMinLimit,
  KasUIntOutOfMinLimit,
  KasULongOutOfMinLimit,    // Cannot happen
  KasFloatOutOfMinLimit,
  KasStringOutOfMinLimit,
  KasCharOutOfMaxLimit,
  KasShortOutOfMaxLimit,
  KasIntOutOfMaxLimit,
  KasLongOutOfMaxLimit,
  KasUCharOutOfMaxLimit,
  KasUShortOutOfMaxLimit,
  KasUIntOutOfMaxLimit,
  KasULongOutOfMaxLimit,    // Cannot happen
  KasFloatOutOfMaxLimit,
  KasStringOutOfMaxLimit,
  KasNameTaken
} KArgsStatus;



// -----------------------------------------------------------------------------
//
// kargsStatus - convert KArgsStatus to English text
//
extern char* kargsStatus(KArgsStatus status);

#endif  // KARGS_KARGSSTATUS_H_

// -----------------------------------------------------------------------------
//
// FILE                  kargsParse.c - parse command line arguments
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//
// ToDo
// - fix mess KArg/karg/kargs
// - Mac & Windows are case insensitive for files and functions etc ...
// - kargsParse - make sure no option is there twice
// - char* kargsVersion(void)
// - Parameters (not options)
//     * Int or String list with min-max no of items
// - Implement kargsConfig(KargsDisable "-v") and add a test case for it
// - Implement kargsConfig(KargsDisableAllBuiltins, NULL) and add a test case for it
// - kargsConfig(KargsEnvVarName, "-v", "VERBOSE")
//     Check both long and short name
// - Two more types for KargsType:
//     * KaIList
//     * KaSList
//     - Their min and max value are integers that describe min and max items in vector
// - incorporate klog: -v -V -t -T, --logDir, --logRotate <method> etc ...
// - Config File
// - JSON Config File?
// - For all external functions:  if kargsInit() hasn't been called: ERROR
// - NEW rules for option dependencies
//   - if 'option X' is set 'option Y' CANNOT be set:    kargsConfig(KargsMutuallyExclusive, "-f", "-y")
//   - if 'option X' is set 'option Y' MUST ALSO be set: kargsConfig((KargsPair, "-f", "-y")
// - logrotate, different ways
//
#include <stdio.h>                     // printf
#include <stdlib.h>                    // exit, strtoll
#include <errno.h>                     // errno
#include <limits.h>                    // min/max values for integer types

#include "kbase/kTypes.h"              // KBool
#include "kbase/kBasicLog.h"           // KBL_*
#include "kbase/kStringSort.h"         // kStringSort

#include "kargs/kargsGlobals.h"	       // kargsProgName, etc
#include "kargs/KArgsStatus.h"         // KArgsStatus
#include "kargs/KArgInfo.h"            // KArgInfo
#include "kargs/KargType.h"            // KargType, KargValueFrom
#include "kargs/KargSort.h"            // KargSort
#include "kargs/KArg.h"                // KArg
#include "kargs/kargsInit.h"           // kargInfoV
#include "kargs/kargsConfig.h"         // Config vars
#include "kargs/kargsUsage.h"          // kargsUsage
#include "kargs/kargsBuiltins.h"       // kargsBuiltins
#include "kargs/KArgsError.h"          // KARGS_ERROR_PUSH
#include "kargs/kargsParse.h"          // Own interface



// -----------------------------------------------------------------------------
//
// max and min values according to types
//
#define MIN_CHAR   SCHAR_MIN
#define	MAX_CHAR   SCHAR_MAX
#define	MAX_UCHAR  UCHAR_MAX

#define MIN_SHORT  SHRT_MIN
#define MAX_SHORT  SHRT_MAX
#define	MAX_USHORT USHRT_MAX

#define MIN_INT    INT_MIN
#define MAX_INT    INT_MAX
#define MAX_UINT   UINT_MAX

#define MIN_LONG   LLONG_MIN
#define MAX_LONG   LLONG_MAX
#define MAX_ULONG  ULLONG_MAX



// -----------------------------------------------------------------------------
//
// isBoolOpt -
//
static KBool isBoolOpt(char shortName, KArgInfo* kargV)
{
  int argIx = 0;

  while (kargV[argIx].type != KaEnd)
  {
    if ((kargV[argIx].shortName != NULL) && (kargV[argIx].shortName[0] == '-') && (shortName == kargV[argIx].shortName[1]))
      return KTRUE;

    ++argIx;
  }

  return KFALSE;
}



// -----------------------------------------------------------------------------
//
// boolOptSet -
//
static void boolOptSet(char shortName, KArgInfo* kiV)
{
  int argIx = 0;

  while (kiV[argIx].type != KaEnd)
  {
    if ((kiV[argIx].shortName != NULL) && (kiV[argIx].shortName[0] == '-') && (shortName == kiV[argIx].shortName[1]))
    {
      *((KBool*) kiV[argIx].valueP) = KTRUE;
      kiV[argIx].from = KavfShortOption;

      return;
    }

    ++argIx;
  }
}



// -----------------------------------------------------------------------------
//
// minMaxValueCheck -
//
// Checking max/min values depending of type of argument
//
// Imagine you have an option -c that is of type KaChar but used like this:
//
// xxx -c 34005
//
// This must give an error, or course
//
static KArgsStatus minMaxValueCheck(KArgInfo* kargP, long long intValue, unsigned long long uintValue)
{
  if ((kargP->type == KaInt8) && ((intValue < MIN_CHAR) || (intValue > MAX_CHAR)))
  {
    printf("%s: value %lld out of range for an int8 option (%s)\n", kargsProgName, intValue, kargP->longName);
    return KasOutOfBounds;
  }
  else if ((kargP->type == KaInt16) && ((intValue < MIN_SHORT) || (intValue > MAX_SHORT)))
  {
    printf("%s: value %lld out of range for an int16 option (%s)\n", kargsProgName, intValue, kargP->longName);
    return KasOutOfBounds;
  }
  else if ((kargP->type == KaInt32) && ((intValue < MIN_INT) || (intValue > MAX_INT)))
  {
    printf("%s: value %lld out of range for an int32 option (%s)\n", kargsProgName, intValue, kargP->longName);
    return KasOutOfBounds;
  }
  else if ((kargP->type == KaUInt8) && ((intValue < 0) || (intValue > MAX_UCHAR)))
  {
    printf("%s: value %lld out of range for a uint8 option (%s)\n", kargsProgName, intValue, kargP->longName);
    return KasOutOfBounds;
  }
  else if ((kargP->type == KaUInt16) && ((intValue < 0) || (intValue > MAX_USHORT)))
  {
    printf("%s: value %lld out of range for a uint16 option (%s)\n", kargsProgName, intValue, kargP->longName);
    return KasOutOfBounds;
  }
  else if ((kargP->type == KaUInt32) && ((intValue < 0) || (intValue > MAX_UINT)))
  {
    printf("%s: value %lld out of range for a uint32 option (%s)\n", kargsProgName, intValue, kargP->longName);
    return KasOutOfBounds;
  }

  return KasOk;
}



// -----------------------------------------------------------------------------
//
// limitCheck - check the limits the user has set for the option
//
KArgsStatus limitCheck(KArgInfo* kargP, long long intValue, unsigned long long uintValue, float floatValue, char* stringValue)
{
  if (kargP->min != KA_NL)
  {
    if ((kargP->type == KaInt8) ||(kargP->type == KaInt16) ||(kargP->type == KaInt32) ||(kargP->type == KaInt64))
    {
      if (intValue < (long long) kargP->min)
      {
        if (kargP->max != KA_NL)
        {
          printf("%s: value %lld for option '%s' is inferior to its lower limit. Allowed range: %lld-%lld\n",
                 kargsProgName,
                 intValue,
                 kargP->longName,
                 (long long) kargP->min, (long long) kargP->max);
        }
        else
          printf("%s: value %lld for option '%s' is inferior to %lld, its lower limit\n",
                 kargsProgName,
                 intValue,
                 kargP->longName,
                 (long long) kargP->min);
                 
        if      (kargP->type == KaInt8)   return KasCharOutOfMinLimit;
        else if (kargP->type == KaInt16)  return KasShortOutOfMinLimit;
        else if (kargP->type == KaInt32)  return KasIntOutOfMinLimit;
        else                              return KasLongOutOfMinLimit;
      }
    }
    else if ((kargP->type == KaUInt8) ||(kargP->type == KaUInt16) ||(kargP->type == KaUInt32) ||(kargP->type == KaUInt64))
    {
      if (uintValue < (unsigned long long) kargP->min)
      {
        if (kargP->max != KA_NL)
          printf("%s: value %lld for option '%s' is inferior to its lower limit. Allowed range: %lld-%lld\n",
                 kargsProgName,
                 intValue,
                 kargP->longName,
                 (long long) kargP->min, (long long) kargP->max);
        else
          printf("%s: value %lld for option '%s' is inferior to %lld, its lower limit\n",
                 kargsProgName,
                 intValue,
                 kargP->longName,
                 (long long) kargP->min);

        if      (kargP->type == KaUInt8)   return KasUCharOutOfMinLimit;
        else if (kargP->type == KaUInt16)  return KasUShortOutOfMinLimit;
        else if (kargP->type == KaUInt32)  return KasUIntOutOfMinLimit;
        else                               return KasULongOutOfMinLimit;  // Not possible
      }
    }
    else if (kargP->type == KaFloat)
    {
      if (floatValue < (float) (long long) kargP->min)
      {
        if (kargP->max != KA_NL)
          printf("%s: value %f for option '%s' is inferior to its lower limit. Allowed range: %f-%f\n",
                 kargsProgName,
                 floatValue,
                 kargP->longName,
                 (float) (long long) kargP->min, (float) (long long) kargP->max);
        else
          printf("%s: value %f for option '%s' is inferior to %f, its lower limit\n",
                 kargsProgName,
                 floatValue,
                 kargP->longName,
                 (float) (long long) kargP->min);
          
        return KasFloatOutOfMinLimit;
      }
    }
    else if (kargP->type == KaString)
    {
      if (strcmp(stringValue, (char*) kargP->min) < 0)
      {
        if (kargP->max != KA_NL)
          printf("%s: value '%s' for option '%s' is inferior to its lower limit. Allowed range: '%s' - '%s'\n",
                 kargsProgName,
                 stringValue,
                 kargP->longName,
                 (char*) kargP->min, (char*) kargP->max);
        else
          printf("%s: value '%s' for option '%s' is inferior to '%s', its lower limit\n",
                 kargsProgName,
                 stringValue,
                 kargP->longName,
                 (char*) kargP->min);
          
        return KasStringOutOfMinLimit;
      }
    }
  }

  if (kargP->max != KA_NL)
  {
    if ((kargP->type == KaInt8) ||(kargP->type == KaInt16) ||(kargP->type == KaInt32) ||(kargP->type == KaInt64))
    {
      if (intValue > (long long) kargP->max)
      {
        if (kargP->min != KA_NL)
          printf("%s: value %lld for option '%s' exceeds its upper limit. Allowed range: %lld-%lld\n",
                 kargsProgName,
                 intValue,
                 kargP->longName,
                 (long long) kargP->min, (long long) kargP->max);
        else
          printf("%s: value %lld for option '%s' exceeds %lld, its upper limit\n",
                 kargsProgName,
                 intValue,
                 kargP->longName,
                 (long long) kargP->max);

        if      (kargP->type == KaInt8)   return KasCharOutOfMaxLimit;
        else if (kargP->type == KaInt16)  return KasShortOutOfMaxLimit;
        else if (kargP->type == KaInt32)  return KasIntOutOfMaxLimit;
        else                              return KasLongOutOfMaxLimit;
      }
    }
    else if ((kargP->type == KaUInt8) ||(kargP->type == KaUInt16) ||(kargP->type == KaUInt32) ||(kargP->type == KaUInt64))
    {
      if (uintValue > (unsigned long long) kargP->max)
      {
        if (kargP->min != KA_NL)
          printf("%s: value %lld for option '%s' exceeds its upper limit. Allowed range: %llu-%llu\n",
                 kargsProgName,
                 intValue,
                 kargP->longName,
                 (unsigned long long) kargP->min, (unsigned long long) kargP->max);
        else
          printf("%s: value %lld for option '%s' exceeds %llu, its upper limit\n",
                 kargsProgName,
                 intValue,
                 kargP->longName,
                 (unsigned long long) kargP->max);

        if      (kargP->type == KaUInt8)   return KasUCharOutOfMaxLimit;
        else if (kargP->type == KaUInt16)  return KasUShortOutOfMaxLimit;
        else if (kargP->type == KaUInt32)  return KasUIntOutOfMaxLimit;
        else                               return KasULongOutOfMaxLimit;  // Not possible
      }
    }
    else if (kargP->type == KaFloat)
    {
      if (floatValue > (float) (long long) kargP->max)
      {
        if (kargP->min != KA_NL)
          printf("%s: value %f for option '%s' exceeds its upper limit. Allowed range: %f-%f\n",
                 kargsProgName,
                 floatValue,
                 kargP->longName,
                 (float) (long long) kargP->min, (float) (long long) kargP->max);
        else
          printf("%s: value %f for option '%s' exceeds %f, its upper limit\n",
                 kargsProgName,
                 floatValue,
                 kargP->longName,
                 (float) (long long) kargP->max);
          
        return KasFloatOutOfMaxLimit;
      }
    }
    else if (kargP->type == KaString)
    {
      if (strcmp(stringValue, (char*) kargP->max) < 0)
      {
        if (kargP->min != KA_NL)
          printf("%s: value '%s' for option '%s' exceeds its upper limit. Allowed range: '%s' - '%s'\n",
                 kargsProgName,
                 stringValue,
                 kargP->longName,
                 (char*) kargP->min, (char*) kargP->max);
        else
          printf("%s: value '%s' for option '%s' exceeds '%s', its upper limit\n",
                 kargsProgName,
                 stringValue,
                 kargP->longName,
                 (char*) kargP->min);
          
        return KasStringOutOfMaxLimit;
      }
    }
  }

  return KasOk;
}



// -----------------------------------------------------------------------------
//
// optionSet -
//
static KArgsStatus optionSet(KArgInfo* kargP, int argC, char* argV[], int argIx)
{
  long long           intValue    = 0;
  unsigned long long  uintValue   = 0;
  float               floatValue  = 0;
  char*               stringValue = NULL;
  KArgsStatus         ks;

  KBL_V(("Setting option '%s', argIx: %d, argC: %d", kargP->longName, argIx, argC));

  if ((kargP->type != KaBool) && (argIx + 1 >= argC))
  {
    printf("%s: value missing for option '%s'\n", kargsProgName, kargP->longName);
    return KasValueMissing;
  }

  if (kargP->type == KaBool)
  {
    *((KBool*) kargP->valueP) = KTRUE;
    return KasOk;
  }    

  if (kargP->type == KaFloat)
  {
    char* endP = NULL;
    
    errno      = 0;
    floatValue = strtof(argV[argIx + 1], &endP);

    if (errno == ERANGE)
    {
      printf("%s: invalid value for float option '%s'\n", kargsProgName, kargP->longName);
      return KasInvalidValue;
    }
  }
  else if (kargP->type == KaString)
    stringValue = argV[argIx + 1];
  else
  {
    int base = 10;

    //
    // Starts with "0", "0x", "H'", "O'", "B'" ?
    //
    // 0x123:  Hexadecimal
    // H'123:  Hexadecimal
    // O'123:  Octadecimal
    // B'101:  Binary
    //
    // 0123:   Octadecimal  ?
    //
    char* valueP = argV[argIx + 1];
    char* rest   = NULL;

    if ((valueP[0] == '0') && ((valueP[1] == 'x') || (valueP[1] == 'X')))
    {
      valueP = &valueP[2];
      base   = 16;
    }
    else if (((valueP[0] == 'H') || (valueP[0] == 'h')) && (valueP[1] == '\''))
    {
      valueP = &valueP[2];
      base   = 16;
    }
    else if (((valueP[0] == 'O') || (valueP[0] == 'o')) && (valueP[1] == '\''))
    {
      valueP = &valueP[2];
      base   = 8;
    }
    else if (((valueP[0] == 'B') || (valueP[0] == 'b')) && (valueP[1] == '\''))
    {
      valueP = &valueP[2];
      base   = 2;
    }

    errno     = 0;
    intValue  = strtoll(valueP,  &rest, base);
    uintValue = strtoull(valueP, &rest, base);

    // KBL_M(("valueP: '%s', base: %d, intValue: %d", valueP, base, intValue));

    if (errno == ERANGE)
    {
      printf("%s: invalid value (%s) for integer option '%s'\n", kargsProgName, valueP, kargP->longName);
      return KasOutOfBounds;
    }

    if ((ks = minMaxValueCheck(kargP, intValue, uintValue)) != KasOk)
      return ks;

    if ((ks = limitCheck(kargP, intValue, uintValue, floatValue, stringValue)) != KasOk)
      return ks;
  }

  //
  // All good, set the value
  //
  if      (kargP->type == KaInt8)     *((char*)               kargP->valueP) = (char)               intValue;
  else if (kargP->type == KaUInt8)    *((unsigned char*)      kargP->valueP) = (unsigned char)      uintValue;
  else if (kargP->type == KaInt16)    *((short*)              kargP->valueP) = (short)              intValue;
  else if (kargP->type == KaUInt16)   *((unsigned short*)     kargP->valueP) = (unsigned short)     uintValue;
  else if (kargP->type == KaInt32)    *((int*)                kargP->valueP) = (int)                intValue;
  else if (kargP->type == KaUInt32)   *((unsigned int*)       kargP->valueP) = (unsigned int)       uintValue;
  else if (kargP->type == KaInt64)    *((long long*)          kargP->valueP) = (long long)          intValue;
  else if (kargP->type == KaUInt64)   *((unsigned long long*) kargP->valueP) = (unsigned long long) uintValue;
  else if (kargP->type == KaFloat)    *((float*)              kargP->valueP) = floatValue;
  else if (kargP->type == KaString)   *((char**)              kargP->valueP) = stringValue;

  return KasOk;
}



// -----------------------------------------------------------------------------
//
// configFileValues -
//
static KArgsStatus configFileValues(KArgInfo* kiV)
{
  return KasOk;
}



// -----------------------------------------------------------------------------
//
// envVarValues -
//
static KArgsStatus envVarValues(KArgInfo* kiV)
{
  return KasOk;
}



// -----------------------------------------------------------------------------
//
// kargsParse -
//
KArgsStatus kargsParse(int argC, char* argV[])
{
  int          argIx = 1;
  KArgsStatus  ks    = KasOk;

  //
  // Set values from config file
  //
  ks = configFileValues(kargInfoV);
  if (ks != KasOk)
  {
    KARGS_ERROR_PUSH("global", ks, "configFileValues error");
    return ks;
  }
  
  //
  // Set values from env vars
  //
  ks = envVarValues(kargInfoV);
  if (ks != KasOk)
  {
    KARGS_ERROR_PUSH("global", ks, "envVarValues error");
    return ks;
  }


  //
  // Set values from command line
  //
  while (argIx < argC)
  {
    int   kargIx = 0;
    KBool found  = KFALSE;
    
    while (kargInfoV[kargIx].type != KaEnd)
    {
      char* optName;

      if ((kargInfoV[kargIx].shortName != NULL) && (strcmp(argV[argIx], kargInfoV[kargIx].shortName) == 0))
      {
        found = KTRUE;
        kargInfoV[kargIx].from = KavfShortOption;
        optName = (char*) kargInfoV[kargIx].shortName;
      }
      else if ((kargInfoV[kargIx].longName != NULL) && (strcmp(argV[argIx], kargInfoV[kargIx].longName) == 0))
      {
        found = KTRUE;
        kargInfoV[kargIx].from = KavfLongOption;
        optName = (char*) kargInfoV[kargIx].longName;
      }

      if (found == KTRUE)
      {
        KBL_V(("Found option '%s': %s", optName, kargInfoV[kargIx].description));
        if ((ks = optionSet(&kargInfoV[kargIx], argC, argV, argIx)) != 0)
        {
          KARGS_ERROR_PUSH(optName, ks, kargsStatus(ks));
          return ks;
        }

        //
        // If not bool option, then the next CLI param is consumed as well
        //
        if (kargInfoV[kargIx].type != KaBool)
          ++argIx;

        break;
      }
      ++kargIx;
    }

    //
    // Concatenated bool options? (like 'ltr' in "ls -ltr")
    //
    if ((found == KFALSE) && (argV[argIx][0] == '-'))
    {
      char*  charsSorted  = strdup(&argV[argIx][1]); // Remove the initial '-'
      char*  toFree       = charsSorted;
      KBool  allOk        = KTRUE;

      kStringSort(charsSorted);

      while (*charsSorted != 0)
      {
        // Check for repeats
        if (charsSorted[0] == charsSorted[1])
        {
          printf("%s: repeated boolean option: -%c\n", kargsProgName, charsSorted[0]);
          exit(1);
        }

        if (isBoolOpt(*charsSorted, kargInfoV) == KFALSE)
        {
          allOk = KFALSE;
          break;
        }
        ++charsSorted;
      }

      if (allOk == KTRUE)
      {
        found = KTRUE;

        charsSorted = &argV[argIx][1];

        while (*charsSorted != 0)
        {
          boolOptSet(*charsSorted, kargInfoV);
          ++charsSorted;
        }
      }

      free(toFree);
    }


    if (found == KFALSE)
    {
      printf("%s: unrecognized option: %s\n", kargsProgName, argV[argIx]);
      kargsUsage();
      exit(1);
    }

    ++argIx;
  }


  //
  // Actions for builtins
  //
  if (kaBuiltinUsage == KTRUE)
  {
    kargsUsage();

    if (kargsExitOnUsage == KTRUE)
      exit(kargsExitOnUsageExitCode);
  }

  if (kaBuiltinExtUsage == KTRUE)
  {
    kargsExtUsage();

    if (kargsExitOnUsage == KTRUE)
      exit(kargsExitOnUsageExitCode);
  }

  return KasOk;
}

// -----------------------------------------------------------------------------
//
// FILE                  kargsBuiltins.c - builtin options of the kargs library
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <stdlib.h>                    // exit

#include "kbase/kTypes.h"              // KBool

#include "kargs/KArg.h"                // KArg, _i, KARGS_END
#include "kargs/kargsBuiltins.h"       // Own interface



// -----------------------------------------------------------------------------
//
// Variables to hold builtins
//
KBool kaBuiltinUsage;
KBool kaBuiltinExtUsage;
KBool kaBuiltinVerbose;
KBool kaBuiltinDebug;



// -----------------------------------------------------------------------------
//
// kargsBuiltins -
//
KArg kargsBuiltins[] =
{
  { "--usage",   "-u", KaBool, &kaBuiltinUsage,    KaOpt, _i KFALSE, _i KFALSE, _i KTRUE, "usage",         },
  { "--Usage",   "-U", KaBool, &kaBuiltinExtUsage, KaOpt, _i KFALSE, _i KFALSE, _i KTRUE, "extended usage" },
  { "--verbose", "-v", KaBool, &kaBuiltinVerbose,  KaOpt, _i KFALSE, _i KFALSE, _i KTRUE, "verbose mode"   },
  { "--debug",   "-d", KaBool, &kaBuiltinDebug,    KaOpt, _i KFALSE, _i KFALSE, _i KTRUE, "debug mode"     },

  KARGS_END
};

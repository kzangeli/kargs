#ifndef KARGS_KARG_H_
#define KARGS_KARG_H_

// -----------------------------------------------------------------------------
//
// FILE                  KArg.h - KArg struct for the kargs library
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <stdio.h>                      // NULL

#include "kargs/KargType.h"             // KargType, KargValueFrom
#include "kargs/KargSort.h"             // KargSort



// -----------------------------------------------------------------------------
//
// KARGS_END - last item in KArg vector
//
#define KARGS_END { "", "", KaEnd, NULL, KaReq, 0, 0, 0, NULL }



// -----------------------------------------------------------------------------
//
// KA_NL - no limit
//
#define KA_NL_NUMBER  0x7FFFFFFFFFFFFFFF
#define KA_NL         (void*) KA_NL_NUMBER



// -----------------------------------------------------------------------------
//
// KArg -
//
typedef struct KArg
{
  const char*  longName;       // Long name of the option, e.g. --port
  const char*  shortName;      // MUST start with '-' or '+'
  KargType     type;           // type: bool, int, string, etc
  void*        valueP;         // points to where to store the value
  KargSort     sort;           // sort: optional, required, hidden
  void*        def;            // default value
  void*        min;            // lower limit
  void*        max;            // upper limit
  const char*  description;    // textual description for usage
} KArg;

#define _i (void*)

#endif  // KARGS_KARG_H_

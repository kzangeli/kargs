#ifndef KARGS_KARGTYPE_H_
#define KARGS_KARGTYPE_H_

// -----------------------------------------------------------------------------
//
// FILE                  KargType.h - KargType enum for Karg and KargInfo structs
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include "kbase/kTypes.h"               // KBool



// -----------------------------------------------------------------------------
//
// ValueFrom -
//
typedef enum KargValueFrom
{
  KavfDefaultValue,
  KavfConfigFile,
  KavfEnvVar,
  KavfShortOption,
  KavfLongOption
} KargValueFrom;



// -----------------------------------------------------------------------------
//
// KargType -
//
typedef enum KargType
{
  KaBool,
  KaFloat,
  KaString,
  KaChar,
  KaInt8    = KaChar,
  KaUChar,
  KaUInt8   = KaUChar,
  KaShort,
  KaInt16   = KaShort,
  KaUShort,
  KaUInt16  = KaUShort,
  KaInt,
  KaInt32   = KaInt,
  KaUInt,
  KaUInt32  = KaUInt,
  KaLong,
  KaInt64   = KaLong,
  KaULong,
  KaUInt64  = KaULong,
  KaEnd
} KargType;

#endif  // KARGS_KARGTYPE_H_

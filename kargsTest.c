// -----------------------------------------------------------------------------
//
// FILE                  kargsTest.c - test program for the kargs library
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <stdio.h>                     // printf, NULL

#include "kbase/kMacros.h"             // K_FT
#include "kbase/kTypes.h"              // KBool
#include "kbase/kBasicLog.h"           // kbVerbose

#include "kargs/kargs.h"               // kargs library



// -----------------------------------------------------------------------------
//
// Variables to hold CLI parameters
//
KBool               ktest;
char                cMin;
char                cMax;
char                cMinMax;
char                charVal;
short               sMin;
short               sMax;
short               sMinMax;
short               shortVal;
int                 iMin;
int                 iMax;
int                 iMinMax;
int                 intVal;
long long           llMin;
long long           llMax;
long long           llMinMax;
long long           longVal;
unsigned char       ucMin;
unsigned char       ucMax;
unsigned char       ucMinMax;
unsigned char       ucharVal;
unsigned short      usMin;
unsigned short      usMax;
unsigned short      usMinMax;
unsigned short      ushortVal;
unsigned int        uiMin;
unsigned int        uiMax;
unsigned int        uiMinMax;
unsigned int        uintVal;
unsigned long long  ullMin;
unsigned long long  ullMax;
unsigned long long  ullMinMax;
unsigned long long  ulongVal;
float               fMin;
float               fMax;
float               fMinMax;
char*               strMin;
char*               strMax;
char*               strMinMax;
KBool               b1;
KBool               b2;
KBool               b3;
KBool               peek;
char*               speek;
char*               testCase;



// -----------------------------------------------------------------------------
//
// kargs - vector of CLI parameters for kargs
//
KArg kargs[] =
{
  { "--ktest",     "-k", KaBool,   &ktest,     KaHid, _i KFALSE,   _i KFALSE, _i KTRUE, "running under ktest test-suite"   },
  { "--case",      NULL, KaString, &testCase,  KaHid, NULL,        KA_NL,     KA_NL,    "Test Case"                        },

  { "--b1",        "-1", KaBool,   &b1,        KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "Bool 1"                           },
  { "--b2",        "-2", KaBool,   &b2,        KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "Bool 2"                           },
  { "--b3",        "-3", KaBool,   &b3,        KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "Bool 3"                           },
  { "--peek",      "-p", KaBool,   &peek,      KaHid, _i KFALSE,   _i KFALSE, _i KTRUE, "Peek"                             },
  { "--speek",     "-s", KaString, &speek,     KaHid, NULL,        KA_NL,     KA_NL,    "String Peek"                      },

  { "--cMin",      NULL, KaChar,   &cMin,      KaOpt, _i 1,        _i -5,     KA_NL,    "char with min limit -5"           },
  { "--cMax",      NULL, KaChar,   &cMax,      KaOpt, _i 1,        KA_NL,     _i 10,    "char with max limit 10"           },
  { "--cMinMax",   NULL, KaChar,   &cMinMax,   KaOpt, _i 1,        _i -5,     _i 10,    "char with limits -5-10"           },
  { "--charVal",   NULL, KaChar,   &charVal,   KaOpt, _i 1,        KA_NL,     KA_NL,    "char without limits"              },

  { "--sMin",      NULL, KaShort,  &sMin,      KaOpt, _i 2,        _i -5,      KA_NL,   "short with min limit -5"          },
  { "--sMax",      NULL, KaShort,  &sMax,      KaOpt, _i 2,        KA_NL,     _i 10,    "short with max limit 10"          },
  { "--sMinMax",   NULL, KaShort,  &sMinMax,   KaOpt, _i 2,        _i -5,      _i 10,   "short with limits -5-10"          },
  { "--shortVal",  NULL, KaShort,  &shortVal,  KaOpt, _i 2,        KA_NL,     KA_NL,    "short without limits"             },

  { "--iMin",      NULL, KaInt,    &iMin,      KaOpt, _i 3,        _i -5,      KA_NL,   "int with min limit -5"            },
  { "--iMax",      NULL, KaInt,    &iMax,      KaOpt, _i 3,        KA_NL,     _i 10,    "int with max limit 10"            },
  { "--iMinMax",   NULL, KaInt,    &iMinMax,   KaOpt, _i 3,        _i -5,      _i 10,   "int with limits -5-10"            },
  { "--intVal",    NULL, KaInt,    &intVal,    KaOpt, _i 3,        KA_NL,     KA_NL,    "int without limits"               },

  { "--llMin",     NULL, KaLong,   &llMin,     KaOpt, _i 4,        _i -5,      KA_NL,   "long with min limit -5"           },
  { "--llMax",     NULL, KaLong,   &llMax,     KaOpt, _i 4,        KA_NL,     _i 10,    "long with max limit 10"           },
  { "--llMinMax",  NULL, KaLong,   &llMinMax,  KaOpt, _i 4,        _i -5,      _i 10,   "long with limits -5-10"           },
  { "--longVal",   NULL, KaLong,   &longVal,   KaOpt, _i 4,        KA_NL,      KA_NL,   "long without limits"              },

  { "--ucMin",     NULL, KaUChar,  &ucMin,     KaOpt, _i 5,        _i 5,      KA_NL,    "unsigned char with min limit 5"   },
  { "--ucMax",     NULL, KaUChar,  &ucMax,     KaOpt, _i 5,        KA_NL,     _i 10,    "unsigned char with max limit 10"  },
  { "--ucMinMax",  NULL, KaUChar,  &ucMinMax,  KaOpt, _i 5,        _i 5,     _i 10,     "unsigned char with limits 5-10"   },

  { "--usMin",     NULL, KaUShort, &usMin,     KaOpt, _i 6,        _i 5,      KA_NL,    "unsigned short with min limit 5"  },
  { "--usMax",     NULL, KaUShort, &usMax,     KaOpt, _i 6,        KA_NL,     _i 10,    "unsigned short with max limit 10" },
  { "--usMinMax",  NULL, KaUShort, &usMinMax,  KaOpt, _i 6,        _i 5,      _i 10,    "unsigned short with limits 5-10"  },

  { "--uiMin",     NULL, KaUInt,   &uiMin,     KaOpt, _i 7,        _i 5,      KA_NL,    "unsigned int with min limit 5"    },
  { "--uiMax",     NULL, KaUInt,   &uiMax,     KaOpt, _i 7,        KA_NL,     _i 10,    "unsigned int with max limit 10"   },
  { "--uiMinMax",  NULL, KaUInt,   &uiMinMax,  KaOpt, _i 7,        _i 5,      _i 10,    "unsigned int with limits 5-10"    },

  { "--ullMin",    NULL, KaULong,  &ullMin,    KaOpt, _i 8,        _i 5,      KA_NL,    "unsigned long with min limit 5"   },
  { "--ullMax",    NULL, KaULong,  &ullMax,    KaOpt, _i 8,        KA_NL,     _i 10,    "unsigned long with max limit 10"  },
  { "--ullMinMax", NULL, KaULong,  &ullMinMax, KaOpt, _i 8,        _i 5,      _i 10,    "unsigned ling with limits 5-10"   },

  { "--fMin",      NULL, KaFloat,  &fMin,      KaOpt, _i 55,       _i 50,     KA_NL,    "float with min limit 50"          },
  { "--fMax",      NULL, KaFloat,  &fMax,      KaOpt, _i 56,       KA_NL,     _i 100,   "float with max limit 100"         },
  { "--fMinMax",   NULL, KaFloat,  &fMinMax,   KaOpt, _i 57,       _i 50,     _i 100,   "float with limits 50-100"         },

  { "--strMin",    NULL, KaString, &strMin,    KaOpt, "060",        _i "050",  KA_NL,    "string with min limit '050'"     },
  { "--strMax",    NULL, KaString, &strMax,    KaOpt, "060",        KA_NL,     _i "100", "string with max limit '100'"     },
  { "--strMinMax", NULL, KaString, &strMinMax, KaOpt, "060",        _i "050",  _i "100", "string with limits '050'-'100'"  },

  KARGS_END
};



// -----------------------------------------------------------------------------
//
// Tests for errors in the KArg vector:
// - min value out of bounds for KaChar    (sMin < MIN_CHAR)     kargsMinCharOutOfBounds                   01
// - min value out of bounds for KaUChar   (sMin is negative)    kargsMinUCharOutOfBounds                  02
// - min value out of bounds for KaShort   (sMin < MIN_SHORT)    kargsMinShortOutOfBounds                  03
// - min value out of bounds for KaUShort  (sMin is negative)    kargsMinUShortOutOfBounds                 04
// - min value out of bounds for KaInt     (sMin < MIN_INT)      kargsMinIntOutOfBounds                    05
// - min value out of bounds for KaUInt    (sMin is negative)    kargsMinUIntOutOfBounds                   06
//
// - max value out of bounds for KaChar    (sMax > MAX_CHAR)     kargsMaxCharOutOfBounds                   07
// - max value out of bounds for KaUChar   (usMax > MAX_UCHAR)   kargsMaxUCharOutOfBounds                  08
// - max value out of bounds for KaShort   (sMax > MAX_SHORT)    kargsMaxShortOutOfBounds                  09
// - max value out of bounds for KaUShort  (usMax > MAX_USHORT)  kargsMaxUShortOutOfBounds                 10
// - max value out of bounds for KaInt     (sMax > MAX_INT)      kargsMaxIntOutOfBounds                    11
// - max value out of bounds for KaUInt    (usMax > MAX_UINT)    kargsMaxUIntOutOfBounds                   12
//
// - def value out of bounds for KaChar    (sDef > MAX_CHAR)     kargsDefCharOutOfMaxBounds                13
// - def value out of bounds for KaChar    (sDef < MIN_CHAR)     kargsDefCharOutOfMinBounds                14
// - def value out of bounds for KaUChar   (usDef > MAX_UCHAR)   kargsDefUCharOutOfMaxBounds               15
// - def value out of bounds for KaUChar   (sDef is negative)    kargsDefUCharOutOfMinBounds               16
// - def value out of bounds for KaShort   (sDef > MAX_SHORT)    kargsDefShortOutOfMaxBounds               17
// - def value out of bounds for KaShort   (sDef < MIN_SHORT)    kargsDefShortOutOfMinBounds               18
// - def value out of bounds for KaUShort  (usDef > MAX_USHORT)  kargsDefUShortOutOfMaxBounds              19
// - def value out of bounds for KaUShort  (sDef is negative)    kargsDefUShortOutOfMinBounds              20
// - def value out of bounds for KaInt     (sDef > MAX_INT)      kargsDefIntOutOfMaxBounds                 21
// - def value out of bounds for KaInt     (sDef < MIN_INT)      kargsDefIntOutOfMinBounds                 22
// - def value out of bounds for KaUInt    (usDef > MAX_UINT)    kargsDefUIntOutOfMaxBounds                23
// - def value out of bounds for KaUInt    (sDef is negative)    kargsDefUIntOutOfMinBounds                24
//
// - char max value < min value                                  kargsCharMaxLimitLessThanMinLimit         25
// - uchar max value < min value                                 kargsUCharMaxLimitLessThanMinLimit        26
// - short max value < min value                                 kargsShortMaxLimitLessThanMinLimit        27
// - ushort max value < min value                                kargsUShortMaxLimitLessThanMinLimit       28
// - int max value < min value                                   kargsIntMaxLimitLessThanMinLimit          29
// - uint max value < min value                                  kargsUIntMaxLimitLessThanMinLimit         30
// - long max value < min value                                  kargsLongMaxLimitLessThanMinLimit         31
// - ulong max value < min value                                 kargsULongMaxLimitLessThanMinLimit        32
// - char def value < min value                                  kargsCharDefValueLessThanMinLimit         33
// - uchar def value < min value                                 kargsUCharDefValueLessThanMinLimit        34
// - short def value < min value                                 kargsShortDefValueLessThanMinLimit        35
// - ushort def value < min value                                kargsUShortDefValueLessThanMinLimit       36
// - int def value < min value                                   kargsIntDefValueLessThanMinLimit          37
// - uint def value < min value                                  kargsUIntDefValueLessThanMinLimit         38
// - long def value < min value                                  kargsLongDefValueLessThanMinLimit         39
// - ulong def value < min value                                 kargsULongDefValueLessThanMinLimit        40
// - char def value > max value                                  kargsCharDefValueExceedsMaxLimit          41
// - uchar def value > max value                                 kargsUCharDefValueExceedsMaxLimit         42
// - short def value > max value                                 kargsShortDefValueExceedsMaxLimit         43
// - ushort def value > max value                                kargsUShortDefValueExceedsMaxLimit        44
// - int def value > max value                                   kargsIntDefValueExceedsMaxLimit           45
// - uint def value > max value                                  kargsUIntDefValueExceedsMaxLimit          46
// - long def value > max value                                  kargsLongDefValueExceedsMaxLimit          47
// - ulong def value > max value                                 kargsULongDefValueExceedsMaxLimit         48
//
// - No longName nor shortName                                   kargsNoOptionName                         49
// - Invalid KargType                                            kargsInvalidType                          50
// - Invalid KargSort                                            kargsInvalidSort                          51
// - NULL valueP                                                 kargsNullValue                            52
// - NULL description                                            kargsNullDescription                      53
//
// - option short-name taken by builtin short-name               kargsShortNameTakenByBuiltinShortName     54
// - option short-name taken by builtin long-name                kargsShortNameTakenByBuiltinLongName      55
// - option long-name taken by builtin short-name                kargsLongNameTakenByBuiltinShortName      56
// - option long-name taken by builtin long-name                 kargsLongNameTakenByBuiltinLongName       57
//
// - option short-name taken by other option short-name          kargsShortNameTakenByOptionShortName      58
// - option short-name taken by other option long-name           kargsShortNameTakenByOptionLongName       59
// - option long-name taken by other option short-name           kargsLongNameTakenByOptionShortName       60
// - option long-name taken by other option long-name            kargsLongNameTakenByOptionLongName        61
//
// - option long name == same option short name                  kargsShortLongNameIsTheSame               62
//
//


// -----------------------------------------------------------------------------
//
// kargsMinCharOutOfBounds (01) - min value out of bounds for KaChar (sMin < MIN_CHAR)
//
KArg kargsMinCharOutOfBounds[] =
{
  { "--MinCharOutOfBounds", "-m", KaChar, &charVal, KaOpt, _i 0, _i -129, _i 10, "--MinCharOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsMinUCharOutOfBounds (02) - min value out of bounds for KaUChar (sMin is negative)
//
KArg kargsMinUCharOutOfBounds[] =
{
  { "--MinUCharOutOfBounds", "-m", KaUChar, &ucharVal, KaOpt, _i 0, _i -1, _i 10, "--MinUCharOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsMinShortOutOfBounds (03) - min value out of bounds for KaShort (sMin < MIN_SHORT)
//
KArg kargsMinShortOutOfBounds[] =
{
  { "--MinShortOutOfBounds", "-m", KaShort, &shortVal, KaOpt, _i 0, _i -100000, _i 10, "--MinShortOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsMinUShortOutOfBounds (04) - min value out of bounds for KaUShort (sMin is negative)
//
KArg kargsMinUShortOutOfBounds[] =
{
  { "--MinUShortOutOfBounds", "-m", KaUShort, &shortVal, KaOpt, _i 0, _i -1, _i 10, "--MinUShortOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsMinIntOutOfBounds (05) - min value out of bounds for KaInt (sMin < MIN_INT)
//
KArg kargsMinIntOutOfBounds[] =
{
  { "--MinIntOutOfBounds", "-m", KaInt, &intVal, KaOpt, _i -2147483649, _i -1, _i 10, "--MinIntOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsMinUIntOutOfBounds (06) - min value out of bounds for KaUInt (sMin is negative)
//
KArg kargsMinUIntOutOfBounds[] =
{
  { "--MinUIntOutOfBounds", "-m", KaUInt, &intVal, KaOpt, _i 1, _i -1, _i 10, "--MinUIntOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsMaxCharOutOfBounds (07) - max value out of bounds for KaChar (sMax > MAX_CHAR)     
//
KArg kargsMaxCharOutOfBounds[] =
{
  { "--MaxCharOutOfBounds", "-m", KaChar, &charVal, KaOpt, _i 1, _i 0, _i 128, "--MaxCharOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsMaxUCharOutOfBounds (08) - max value out of bounds for KaUChar (usMax > MAX_UCHAR)
//
KArg kargsMaxUCharOutOfBounds[] =
{
  { "--MaxUCharOutOfBounds", "-m", KaUChar, &ucharVal, KaOpt, _i 1, _i 0, _i 256, "--MaxUCharOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsMaxShortOutOfBounds (09) - max value out of bounds for KaShort (sMax > MAX_SHORT)
//
KArg kargsMaxShortOutOfBounds[] =
{
  { "--MaxShortOutOfBounds", "-m", KaShort, &shortVal, KaOpt, _i 1, _i 0, _i 40000, "--MaxShortOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsMaxUShortOutOfBounds (10) - max value out of bounds for KaUShort (usMax > MAX_USHORT)
//
KArg kargsMaxUShortOutOfBounds[] =
{
  { "--MaxUShortOutOfBounds", "-m", KaUShort, &ushortVal, KaOpt, _i 1, _i 0, _i 66000, "--MaxUShortOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsMaxIntOutOfBounds (11) - max value out of bounds for KaInt (sMax > MAX_INT)
//
KArg kargsMaxIntOutOfBounds[] =
{
  { "--MaxIntOutOfBounds", "-m", KaInt, &intVal, KaOpt, _i 1, _i 0, _i 0xFFFFFFFFF, "--MaxIntOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsMaxUIntOutOfBounds (12) - max value out of bounds for KaUInt (usMax > MAX_UINT)
//
KArg kargsMaxUIntOutOfBounds[] =
{
  { "--MaxUIntOutOfBounds", "-m", KaUInt, &uintVal, KaOpt, _i 1, _i 0, _i 0xFFFFFFFFF, "--MaxUIntOutOfBounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefCharOutOfMaxBounds (13) - def value out of bounds for KaChar (sDef > MAX_CHAR)
//
KArg kargsDefCharOutOfMaxBounds[] =
{
  { "--DefCharOutOfMaxBounds", NULL, KaChar, &charVal, KaOpt, _i 128, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefCharOutOfMinBounds (14) - def value out of bounds for KaChar (sDef < MIN_CHAR)
//
KArg kargsDefCharOutOfMinBounds[] =
{
  { "--DefCharOutOfMinBounds", NULL, KaChar, &charVal, KaOpt, _i -129, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefUCharOutOfMaxBounds (15) - def value out of bounds for KaUChar (usDef > MAX_UCHAR)
//
KArg kargsDefUCharOutOfMaxBounds[] =
{
  { "--DefUCharOutOfMaxBounds", NULL, KaUChar, &ucharVal, KaOpt, _i 256, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefUCharOutOfMinBounds (16) - def value out of bounds for KaUChar (sDef is negative)
//
KArg kargsDefUCharOutOfMinBounds[] =
{
  { "--DefUCharOutOfMinBounds", NULL, KaUChar, &ucharVal, KaOpt, _i -1, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefShortOutOfMaxBounds (17) - def value out of bounds for KaShort (sDef > MAX_SHORT)
//
KArg kargsDefShortOutOfMaxBounds[] =
{
  { "--DefShortOutOfMaxBounds", NULL, KaShort, &shortVal, KaOpt, _i 36000, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefShortOutOfMinBounds END(18) - def value out of bounds for KaShort (sDef < MIN_SHORT)
//
KArg kargsDefShortOutOfMinBounds[] =
{
  { "--DefShortOutOfMinBounds", NULL, KaShort, &shortVal, KaOpt, _i -36000, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefUShortOutOfMaxBounds (19) - def value out of bounds for KaUShort (usDef > MAX_USHORT)
//
KArg kargsDefUShortOutOfMaxBounds[] =
{
  { "--DefUShortOutOfMaxBounds", NULL, KaUShort, &ushortVal, KaOpt, _i 66000, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefUShortOutOfMinBounds (20) - def value out of bounds for KaUShort (sDef is negative)
//
KArg kargsDefUShortOutOfMinBounds[] =
{
  { "--DefUShortOutOfMinBounds", NULL, KaUShort, &ushortVal, KaOpt, _i -1, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefIntOutOfMaxBounds (21) - def value out of bounds for KaInt (sDef > MAX_INT)
//
KArg kargsDefIntOutOfMaxBounds[] =
{
  { "--DefIntOutOfMaxBounds", NULL, KaInt, &intVal, KaOpt, _i 3000000000, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefIntOutOfMinBounds (22) - def value out of bounds for KaInt (sDef < MIN_INT)
//
KArg kargsDefIntOutOfMinBounds[] =
{
  { "--DefIntOutOfMinBounds", NULL, KaInt, &intVal, KaOpt, _i -3000000000, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefUIntOutOfMaxBounds (23) - def value out of bounds for KaUInt (usDef > MAX_UINT)
//
KArg kargsDefUIntOutOfMaxBounds[] =
{
  { "--DefUIntOutOfMaxBounds", NULL, KaUInt, &uintVal, KaOpt, _i 5000000000, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsDefUIntOutOfMinBounds (24) - def value out of bounds for KaUInt (sDef is negative)
//
KArg kargsDefUIntOutOfMinBounds[] =
{
  { "--DefUIntOutOfMinBounds", NULL, KaUInt, &uintVal, KaOpt, _i -1, KA_NL, KA_NL, "default value out of bounds" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsCharMaxLimitLessThanMinLimit (25) - char max value < min value                                  
//
KArg kargsCharMaxLimitLessThanMinLimit[] =
{
  { "--CharMaxLimitLessThanMinLimit", NULL, KaChar, &charVal, KaOpt, _i 22, _i 25, _i 20, "CharMaxLimitLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsUCharMaxLimitLessThanMinLimit (26) - uchar max value < min value
//
KArg kargsUCharMaxLimitLessThanMinLimit[] =
{
  { "--UCharMaxLimitLessThanMinLimit", NULL, KaUChar, &ucharVal, KaOpt, _i 22, _i 25, _i 20, "UCharMaxLimitLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsShortMaxLimitLessThanMinLimit (27) - short max value < min value                                 
//
KArg kargsShortMaxLimitLessThanMinLimit[] =
{
  { "--ShortMaxLimitLessThanMinLimit", NULL, KaShort, &shortVal, KaOpt, _i 22, _i 25, _i 20, "ShortMaxLimitLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsUShortMaxLimitLessThanMinLimit (28) - ushort max value < min value                                
//
KArg kargsUShortMaxLimitLessThanMinLimit[] =
{
  { "--UShortMaxLimitLessThanMinLimit", NULL, KaUShort, &ushortVal, KaOpt, _i 22, _i 25, _i 20, "UShortMaxLimitLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsIntMaxLimitLessThanMinLimit (29) - int max value < min value                                   
//
KArg kargsIntMaxLimitLessThanMinLimit[] =
{
  { "--IntMaxLimitLessThanMinLimit", NULL, KaInt, &intVal, KaOpt, _i 22, _i 25, _i 20, "IntMaxLimitLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsUIntMaxLimitLessThanMinLimit (30) - uint max value < min value                                  
//
KArg kargsUIntMaxLimitLessThanMinLimit[] =
{
  { "--UIntMaxLimitLessThanMinLimit", NULL, KaUInt, &uintVal, KaOpt, _i 22, _i 25, _i 20, "UIntMaxLimitLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsLongMaxLimitLessThanMinLimit (31) - long max value < min value
//
KArg kargsLongMaxLimitLessThanMinLimit[] =
{
  { "--LongMaxLimitLessThanMinLimit", NULL, KaLong, &longVal, KaOpt, _i 22, _i 25, _i 20, "LongMaxLimitLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsULongMaxLimitLessThanMinLimit (32) - ulong max value < min value
//
KArg kargsULongMaxLimitLessThanMinLimit[] =
{
  { "--ULongMaxLimitLessThanMinLimit", NULL, KaULong, &ulongVal, KaOpt, _i 22, _i 25, _i 20, "ULongMaxLimitLessThanMinLimit" },
  KARGS_END
};




// -----------------------------------------------------------------------------
//
// kargsCharDefValueLessThanMinLimit (33) - char def value < min value
//
KArg kargsCharDefValueLessThanMinLimit[] =
{
  { "--CharDefValueLessThanMinLimit", NULL, KaChar, &charVal, KaOpt, _i 9, _i 10, _i 20, "CharDefValueLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsUCharDefValueLessThanMinLimit (34) - uchar def value < min value                                 
//
KArg kargsUCharDefValueLessThanMinLimit[] =
{
  { "--UCharDefValueLessThanMinLimit", NULL, KaUChar, &ucharVal, KaOpt, _i 9, _i 10, _i 20, "UCharDefValueLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsShortDefValueLessThanMinLimit (35) - short def value < min value                                 
//
KArg kargsShortDefValueLessThanMinLimit[] =
{
  { "--ShortDefValueLessThanMinLimit", NULL, KaShort, &shortVal, KaOpt, _i 9, _i 10, _i 20, "ShortDefValueLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsUShortDefValueLessThanMinLimit (36) - ushort def value < min value                                
//
KArg kargsUShortDefValueLessThanMinLimit[] =
{
  { "--UShortDefValueLessThanMinLimit", NULL, KaUShort, &ushortVal, KaOpt, _i 9, _i 10, _i 20, "UShortDefValueLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsIntDefValueLessThanMinLimit (37) - int def value < min value                                   
//
KArg kargsIntDefValueLessThanMinLimit[] =
{
  { "--IntDefValueLessThanMinLimit", NULL, KaInt, &intVal, KaOpt, _i 9, _i 10, _i 20, "IntDefValueLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsUIntDefValueLessThanMinLimit (38) - uint def value < min value
//
KArg kargsUIntDefValueLessThanMinLimit[] =
{
  { "--UIntDefValueLessThanMinLimit", NULL, KaUInt, &uintVal, KaOpt, _i 9, _i 10, _i 20, "UIntDefValueLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsLongDefValueLessThanMinLimit (39) - long def value < min value
//
KArg kargsLongDefValueLessThanMinLimit[] =
{
  { "--LongDefValueLessThanMinLimit", NULL, KaLong, &longVal, KaOpt, _i 9, _i 10, _i 20, "LongDefValueLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsULongDefValueLessThanMinLimit (40) - ulong def value < min value
//
KArg kargsULongDefValueLessThanMinLimit[] =
{
  { "--ULongDefValueLessThanMinLimit", NULL, KaULong, &ulongVal, KaOpt, _i 9, _i 10, _i 20, "ULongDefValueLessThanMinLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsCharDefValueExceedsMaxLimit (41) - char def value > max value                                  
//
KArg kargsCharDefValueExceedsMaxLimit[] =
{
  { "--CharDefValueExceedsMaxLimit", NULL, KaChar, &charVal, KaOpt, _i 22, _i 10, _i 20, "CharDefValueExceedsMaxLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsUCharDefValueExceedsMaxLimit (42) - uchar def value > max value                                 
//
KArg kargsUCharDefValueExceedsMaxLimit[] =
{
  { "--UCharDefValueExceedsMaxLimit", NULL, KaUChar, &ucharVal, KaOpt, _i 22, _i 10, _i 20, "UCharDefValueExceedsMaxLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsShortDefValueExceedsMaxLimit (43) - short def value > max value                                 
//
KArg kargsShortDefValueExceedsMaxLimit[] =
{
  { "--ShortDefValueExceedsMaxLimit", NULL, KaShort, &shortVal, KaOpt, _i 22, _i 10, _i 20, "ShortDefValueExceedsMaxLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsUShortDefValueExceedsMaxLimit (44) - ushort def value > max value                                
//
KArg kargsUShortDefValueExceedsMaxLimit[] =
{
  { "--UShortDefValueExceedsMaxLimit", NULL, KaUShort, &ushortVal, KaOpt, _i 22, _i 10, _i 20, "UShortDefValueExceedsMaxLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsIntDefValueExceedsMaxLimit (45) - int def value > max value                                   
//
KArg kargsIntDefValueExceedsMaxLimit[] =
{
  { "--IntDefValueExceedsMaxLimit", NULL, KaInt, &intVal, KaOpt, _i 22, _i 10, _i 20, "IntDefValueExceedsMaxLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsUIntDefValueExceedsMaxLimit (46) - uint def value > max value                                  
//
KArg kargsUIntDefValueExceedsMaxLimit[] =
{
  { "--UIntDefValueExceedsMaxLimit", NULL, KaUInt, &uintVal, KaOpt, _i 22, _i 10, _i 20, "UIntDefValueExceedsMaxLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsLongDefValueExceedsMaxLimit (47) - long def value > max value
//
KArg kargsLongDefValueExceedsMaxLimit[] =
{
  { "--LongDefValueExceedsMaxLimit", NULL, KaLong, &longVal, KaOpt, _i 22, _i 10, _i 20, "LongDefValueExceedsMaxLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsULongDefValueExceedsMaxLimit (48) - ulong def value > max value
//
KArg kargsULongDefValueExceedsMaxLimit[] =
{
  { "--ULongDefValueExceedsMaxLimit", NULL, KaULong, &ulongVal, KaOpt, _i 22, _i 10, _i 20, "ULongDefValueExceedsMaxLimit" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsNoOptionName (49) - no longName nor shortName
//
KArg kargsNoOptionName[] =
{
  { NULL, NULL, KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "no option name" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsInvalidType (50) - Invalid KargType                                            
//
KArg kargsInvalidType[] =
{
  { "--InvalidType", NULL, (KargType) 101, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "Invalid KargType" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsInvalidSort (51) - Invalid KargSort                                            
//
KArg kargsInvalidSort[] =
{
  { "--InvalidSort", NULL, KaBool, &b1, (KargSort) 101, _i KFALSE,   _i KFALSE, _i KTRUE, "Invalid KargSort" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsNullValue (52) - NULL valueP                                                 
//
KArg kargsNullValue[] =
{
  { "--NullValue", NULL, KaBool, NULL, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "NULL valueP" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsNullDescription (53) - NULL description                                            
//
KArg kargsNullDescription[] =
{
  { "--NullDescription", NULL, KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, NULL },
  KARGS_END
};




// -----------------------------------------------------------------------------
//
// kargsShortNameTakenByBuiltinShortName (54) -
//
KArg kargsShortNameTakenByBuiltinShortName[] =
{
  { "--XX", "-u", KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "-u" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsShortNameTakenByBuiltinLongName (55) -
//
KArg kargsShortNameTakenByBuiltinLongName[] =
{
  { NULL, "--usage", KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "-u" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsLongNameTakenByBuiltinShortName (56) -
//
KArg kargsLongNameTakenByBuiltinShortName[] =
{
  { "-u", NULL, KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "--usage" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsLongNameTakenByBuiltinLongName (57) -
//
KArg kargsLongNameTakenByBuiltinLongName[] =
{
  { "--usage", NULL, KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "--usage" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsShortNameTakenByOptionShortName (58) -
//
KArg kargsShortNameTakenByOptionShortName[] =
{
  { NULL, "-s", KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "-s as shortName" },
  { NULL, "-s", KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "-s as shortName" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsShortNameTakenByOptionLongName (59) -
//
KArg kargsShortNameTakenByOptionLongName[] =
{
  { "-b",    "-1", KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "--bool as longName" },
  { "-bool", "-b", KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "--bool as longName" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsLongNameTakenByOptionShortName (60) -
//
KArg kargsLongNameTakenByOptionShortName[] =
{
  { "--ss", "-s", KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "-s as shortName" },
  { "-s", " -s2", KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "-s as longName"  },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsLongNameTakenByOptionLongName (61) -
//
KArg kargsLongNameTakenByOptionLongName[] =
{
  { "--bool", NULL, KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "--bool as longName" },
  { "--bool", NULL, KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "--bool as longName" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// kargsShortLongNameIsTheSame (62) - this one should work
//
KArg kargsShortLongNameIsTheSame[] =
{
  { "-b", "-b", KaBool, &b1, KaOpt, _i KFALSE,   _i KFALSE, _i KTRUE, "-b as both short and long name" },
  KARGS_END
};



// -----------------------------------------------------------------------------
//
// main - 
//
static void kargsErrorPresent(void)
{
  KArgsError* eP = kargsErrorGet();
  int         eNo = 1;

  while (eP != NULL)
  {
    if (kbVerbose)
      printf("%s[%d]:%s: %s: %s\n", eP->fileName, eP->lineNo, eP->funcName, eP->optionName, eP->description);
    else
      printf("Error %d: %s: %s\n", eNo, eP->optionName, eP->description);

    eP = eP->next;
    ++eNo;
  }
}



// -----------------------------------------------------------------------------
//
// main - 
//
int main(int argC, char* argV[])
{
  KArgsStatus  ks;
  char*        peekValue;
  
  if (kargsPeek(argC, argV, kargs, "--ktest") != NULL)
    kbNoLineNumbers = KTRUE;

  if (kargsPeek(argC, argV, kargs, "-v") != NULL)
    kbVerbose = KTRUE;
  KBL_V(("Calling kargsPeek"));

  if (kargsPeek(argC, argV, kargs, "--peek") != NULL)
  {
    printf("--peek is SET\n");
    return 0;
  }
  
  KBL_V(("Calling kargsPeek again"));

  if ((peekValue = kargsPeek(argC, argV, kargs, "--speek")) != NULL)
  {
    printf("--speek is '%s'\n", peekValue);
    return 0;
  }

  if ((peekValue = kargsPeek(argC, argV, kargs, "--case")) != NULL)
  {
    if (strcmp(peekValue, "MinCharOutOfBounds") == 0)
    {
      char* caseName = "MinCharOutOfBounds";

      ks = kargsInit("kargsTest", kargsMinCharOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "MinUCharOutOfBounds") == 0)
    {
      char* caseName = "MinUCharOutOfBounds";

      ks = kargsInit("kargsTest", kargsMinUCharOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "MinShortOutOfBounds") == 0)
    {
      char* caseName = "MinShortOutOfBounds";

      ks = kargsInit("kargsTest", kargsMinShortOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "MinUShortOutOfBounds") == 0)
    {
      char* caseName = "MinUShortOutOfBounds";

      ks = kargsInit("kargsTest", kargsMinUShortOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "MinIntOutOfBounds") == 0)
    {
      char* caseName = "MinIntOutOfBounds";

      ks = kargsInit("kargsTest", kargsMinIntOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "MinUIntOutOfBounds") == 0)
    {
      char* caseName = "MinUIntOutOfBounds";

      ks = kargsInit("kargsTest", kargsMinUIntOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "MaxCharOutOfBounds") == 0)
    {
      char* caseName = "MaxCharOutOfBounds";

      ks = kargsInit("kargsTest", kargsMaxCharOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "MaxUCharOutOfBounds") == 0)
    {
      char* caseName = "MaxUCharOutOfBounds";

      ks = kargsInit("kargsTest", kargsMaxUCharOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "MaxShortOutOfBounds") == 0)
    {
      char* caseName = "MaxShortOutOfBounds";

      ks = kargsInit("kargsTest", kargsMaxShortOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "MaxUShortOutOfBounds") == 0)
    {
      char* caseName = "MaxUShortOutOfBounds";

      ks = kargsInit("kargsTest", kargsMaxUShortOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "MaxIntOutOfBounds") == 0)
    {
      char* caseName = "MaxIntOutOfBounds";

      ks = kargsInit("kargsTest", kargsMaxIntOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "MaxUIntOutOfBounds") == 0)
    {
      char* caseName = "MaxUIntOutOfBounds";

      ks = kargsInit("kargsTest", kargsMaxUIntOutOfBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefCharOutOfMaxBounds") == 0)
    {
      char* caseName = "DefCharOutOfMaxBounds";

      ks = kargsInit("kargsTest", kargsDefCharOutOfMaxBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefCharOutOfMinBounds") == 0)
    {
      char* caseName = "DefCharOutOfMinBounds";

      ks = kargsInit("kargsTest", kargsDefCharOutOfMinBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefUCharOutOfMaxBounds") == 0)
    {
      char* caseName = "DefUCharOutOfMaxBounds";

      ks = kargsInit("kargsTest", kargsDefUCharOutOfMaxBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefUCharOutOfMinBounds") == 0)
    {
      char* caseName = "DefUCharOutOfMinBounds";

      ks = kargsInit("kargsTest", kargsDefUCharOutOfMinBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefShortOutOfMaxBounds") == 0)
    {
      char* caseName = "DefShortOutOfMaxBounds";

      ks = kargsInit("kargsTest", kargsDefShortOutOfMaxBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefShortOutOfMinBounds") == 0)
    {
      char* caseName = "DefShortOutOfMinBounds";

      ks = kargsInit("kargsTest", kargsDefShortOutOfMinBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefUShortOutOfMaxBounds") == 0)
    {
      char* caseName = "DefUShortOutOfMaxBounds";

      ks = kargsInit("kargsTest", kargsDefUShortOutOfMaxBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefUShortOutOfMinBounds") == 0)
    {
      char* caseName = "DefUShortOutOfMinBounds";

      ks = kargsInit("kargsTest", kargsDefUShortOutOfMinBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefIntOutOfMaxBounds") == 0)
    {
      char* caseName = "DefIntOutOfMaxBounds";

      ks = kargsInit("kargsTest", kargsDefIntOutOfMaxBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefIntOutOfMinBounds") == 0)
    {
      char* caseName = "DefIntOutOfMinBounds";

      ks = kargsInit("kargsTest", kargsDefIntOutOfMinBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefUIntOutOfMaxBounds") == 0)
    {
      char* caseName = "DefUIntOutOfMaxBounds";

      ks = kargsInit("kargsTest", kargsDefUIntOutOfMaxBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "DefUIntOutOfMinBounds") == 0)
    {
      char* caseName = "DefUIntOutOfMinBounds";

      ks = kargsInit("kargsTest", kargsDefUIntOutOfMinBounds, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }


    else if (strcmp(peekValue, "CharMaxLimitLessThanMinLimit") == 0)
    {
      char* caseName = "CharMaxLimitLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsCharMaxLimitLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "UCharMaxLimitLessThanMinLimit") == 0)
    {
      char* caseName = "UCharMaxLimitLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsUCharMaxLimitLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "ShortMaxLimitLessThanMinLimit") == 0)
    {
      char* caseName = "ShortMaxLimitLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsShortMaxLimitLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "UShortMaxLimitLessThanMinLimit") == 0)
    {
      char* caseName = "UShortMaxLimitLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsUShortMaxLimitLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "IntMaxLimitLessThanMinLimit") == 0)
    {
      char* caseName = "IntMaxLimitLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsIntMaxLimitLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "UIntMaxLimitLessThanMinLimit") == 0)
    {
      char* caseName = "UIntMaxLimitLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsUIntMaxLimitLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "LongMaxLimitLessThanMinLimit") == 0)
    {
      char* caseName = "LongMaxLimitLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsLongMaxLimitLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "ULongMaxLimitLessThanMinLimit") == 0)
    {
      char* caseName = "ULongMaxLimitLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsULongMaxLimitLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "CharDefValueLessThanMinLimit") == 0)
    {
      char* caseName = "CharDefValueLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsCharDefValueLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "UCharDefValueLessThanMinLimit") == 0)
    {
      char* caseName = "UCharDefValueLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsUCharDefValueLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "ShortDefValueLessThanMinLimit") == 0)
    {
      char* caseName = "ShortDefValueLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsShortDefValueLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "UShortDefValueLessThanMinLimit") == 0)
    {
      char* caseName = "UShortDefValueLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsUShortDefValueLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "IntDefValueLessThanMinLimit") == 0)
    {
      char* caseName = "IntDefValueLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsIntDefValueLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "IntDefValueLessThanMinLimit") == 0)
    {
      char* caseName = "IntDefValueLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsIntDefValueLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "UIntDefValueLessThanMinLimit") == 0)
    {
      char* caseName = "UIntDefValueLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsUIntDefValueLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "LongDefValueLessThanMinLimit") == 0)
    {
      char* caseName = "LongDefValueLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsLongDefValueLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "ULongDefValueLessThanMinLimit") == 0)
    {
      char* caseName = "ULongDefValueLessThanMinLimit";

      ks = kargsInit("kargsTest", kargsULongDefValueLessThanMinLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "CharDefValueExceedsMaxLimit") == 0)
    {
      char* caseName = "CharDefValueExceedsMaxLimit";

      ks = kargsInit("kargsTest", kargsCharDefValueExceedsMaxLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "UCharDefValueExceedsMaxLimit") == 0)
    {
      char* caseName = "UCharDefValueExceedsMaxLimit";

      ks = kargsInit("kargsTest", kargsUCharDefValueExceedsMaxLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "ShortDefValueExceedsMaxLimit") == 0)
    {
      char* caseName = "ShortDefValueExceedsMaxLimit";

      ks = kargsInit("kargsTest", kargsShortDefValueExceedsMaxLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "UShortDefValueExceedsMaxLimit") == 0)
    {
      char* caseName = "UShortDefValueExceedsMaxLimit";

      ks = kargsInit("kargsTest", kargsUShortDefValueExceedsMaxLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "IntDefValueExceedsMaxLimit") == 0)
    {
      char* caseName = "IntDefValueExceedsMaxLimit";

      ks = kargsInit("kargsTest", kargsIntDefValueExceedsMaxLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "UIntDefValueExceedsMaxLimit") == 0)
    {
      char* caseName = "UIntDefValueExceedsMaxLimit";

      ks = kargsInit("kargsTest", kargsUIntDefValueExceedsMaxLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "LongDefValueExceedsMaxLimit") == 0)
    {
      char* caseName = "LongDefValueExceedsMaxLimit";

      ks = kargsInit("kargsTest", kargsLongDefValueExceedsMaxLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "ULongDefValueExceedsMaxLimit") == 0)
    {
      char* caseName = "ULongDefValueExceedsMaxLimit";

      ks = kargsInit("kargsTest", kargsULongDefValueExceedsMaxLimit, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "NoOptionName") == 0)
    {
      char* caseName = "NoOptionName";

      ks = kargsInit("kargsTest", kargsNoOptionName, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "InvalidType") == 0)
    {
      char* caseName = "InvalidType";

      ks = kargsInit("kargsTest", kargsInvalidType, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "InvalidSort") == 0)
    {
      char* caseName = "InvalidSort";

      ks = kargsInit("kargsTest", kargsInvalidSort, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "NullValue") == 0)
    {
      char* caseName = "NullValue";

      ks = kargsInit("kargsTest", kargsNullValue, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "NullDescription") == 0)
    {
      char* caseName = "NullDescription";

      ks = kargsInit("kargsTest", kargsNullDescription, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "ShortNameTakenByBuiltinShortName") == 0)
    {
      char* caseName = "ShortNameTakenByBuiltinShortName";

      ks = kargsInit("kargsTest", kargsShortNameTakenByBuiltinShortName, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "ShortNameTakenByBuiltinLongName") == 0)
    {
      char* caseName = "ShortNameTakenByBuiltinLongName";

      ks = kargsInit("kargsTest", kargsShortNameTakenByBuiltinLongName, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "LongNameTakenByBuiltinShortName") == 0)
    {
      char* caseName = "LongNameTakenByBuiltinShortName";

      ks = kargsInit("kargsTest", kargsLongNameTakenByBuiltinShortName, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "LongNameTakenByBuiltinLongName") == 0)
    {
      char* caseName = "LongNameTakenByBuiltinLongName";

      ks = kargsInit("kargsTest", kargsLongNameTakenByBuiltinLongName, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "ShortNameTakenByOptionShortName") == 0)
    {
      char* caseName = "ShortNameTakenByOptionShortName";

      ks = kargsInit("kargsTest", kargsShortNameTakenByOptionShortName, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }
      
      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "ShortNameTakenByOptionLongName") == 0)
    {
      char* caseName = "ShortNameTakenByOptionLongName";

      ks = kargsInit("kargsTest", kargsShortNameTakenByOptionLongName, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }

      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "LongNameTakenByOptionShortName") == 0)
    {
      char* caseName = "LongNameTakenByOptionShortName";

      ks = kargsInit("kargsTest", kargsLongNameTakenByOptionShortName, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }

      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "LongNameTakenByOptionLongName") == 0)
    {
      char* caseName = "LongNameTakenByOptionLongName";

      ks = kargsInit("kargsTest", kargsLongNameTakenByOptionLongName, "KARGSTEST");
      if (ks == KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit did not detect %s", caseName));
      }

      printf("OK: %s error correctly detected\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
    else if (strcmp(peekValue, "ShortLongNameIsTheSame") == 0)
    {
      char* caseName = "ShortLongNameIsTheSame";

      ks = kargsInit("kargsTest", kargsShortLongNameIsTheSame, "KARGSTEST");
      if (ks != KasOk)
      {
        kargsErrorPresent();
        KBL_X(1, ("kargsInit incorrectly flagged error for %s", caseName));
      }
      
      printf("OK: no error for %s\n", caseName);
      kargsErrorPresent();
      exit(0);
    }
  }

  KBL_V(("Calling kargsInit"));
  ks = kargsInit("kargsTest", kargs, "KARGSTEST");
  if (ks != KasOk)
  {
    kargsErrorPresent();
    KBL_X(1, ("error initializing kargs library (error %d)", ks));
  }

  KBL_V(("Calling kargsConfig"));
  kargsConfig(KargsPrefix, "KAT_");

  KBL_V(("Calling kargsParse"));
  ks = kargsParse(argC, argV);
  KBL_V(("Back from kargsParse"));

  if (ks != KasOk)
  {
    kargsErrorPresent();
    exit(1);
  }

  // kargsPeek("-u");


  printf("cMin:      %d\n", cMin);
  printf("cMax:      %d\n", cMax);
  printf("cMinMax:   %d\n", cMinMax);
  printf("charVal:   %d\n", charVal);
  
  printf("sMin:      %d\n", sMin);
  printf("sMax:      %d\n", sMax);
  printf("sMinMax:   %d\n", sMinMax);
  printf("shortVal:  %d\n", shortVal);

  printf("iMin:      %d\n", iMin);
  printf("iMax:      %d\n", iMax);
  printf("iMinMax:   %d\n", iMinMax);
  printf("intVal:    %d\n", intVal);

  printf("llMin:     %lld\n", llMin);
  printf("llMax:     %lld\n", llMax);
  printf("llMinMax:  %lld\n", llMinMax);
  printf("longVal:   %lld\n", longVal);

  printf("ucMin:     %d\n", ucMin);
  printf("ucMax:     %d\n", ucMax);
  printf("ucMinMax:  %d\n", ucMinMax);

  printf("usMin:     %d\n", usMin);
  printf("usMax:     %d\n", usMax);
  printf("usMinMax:  %d\n", usMinMax);

  printf("uiMin:     %d\n", uiMin);
  printf("uiMax:     %d\n", uiMax);
  printf("uiMinMax:  %d\n", uiMinMax);

  printf("ullMin:    %llu\n", ullMin);
  printf("ullMax:    %llu\n", ullMax);
  printf("ullMinMax: %llu\n", ullMinMax);

  printf("fMin:      %f\n", fMin);
  printf("fMax:      %f\n", fMax);
  printf("fMinMax:   %f\n", fMinMax);

  printf("strMin:    %s\n", strMin);
  printf("strMax:    %s\n", strMax);
  printf("strMinMax: %s\n", strMinMax);

  printf("b1:        %s\n", K_FT(b1));
  printf("b2:        %s\n", K_FT(b2));
  printf("b3:        %s\n", K_FT(b3));

  return 0;
}

// -----------------------------------------------------------------------------
//
// FILE                  kargsUsage.c - handling of the usage CLI
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <stdio.h>                     // printf, NULL
#include <string.h>                    // strdup

#include "kbase/kTypes.h"              // KBool
#include "kbase/kBasicLog.h"           // KBL_*

#include "kargs/KArgInfo.h"            // KArgInfo
#include "kargs/kargsGlobals.h"        // kargsProgName, etc
#include "kargs/kargsBuiltins.h"       // kaBuiltins
#include "kargs/kargsInit.h"           // kargInfoV
#include "kargs/kargsUsage.h"          // Own interface



// -----------------------------------------------------------------------------
//
// kargsUsage -
//
void kargsUsage(void)
{
  char spaces[128];
  int  len = 7 + strlen(kargsProgName) + 2;

  KBL_V(("In"));

  // Protect against ridiculously long program names
  if (len >= sizeof(spaces))
    len = 2;
      
  memset(spaces, ' ', len);
  spaces[len] = 0;

  int    ix                 = 0;
  KBool  usageStringPrinted = KFALSE;

  while (kargInfoV[ix].type != KaEnd)
  {
    KBool  hasLongName  = KTRUE;
    KBool  hasShortName = KTRUE;

    if ((kargInfoV[ix].sort == KaHid) || (kargInfoV[ix].disabled == KTRUE))
    {
      ++ix;
      continue;
    }

    if ((kargInfoV[ix].longName == NULL) || (kargInfoV[ix].longName[0] == 0))
      hasLongName = KFALSE;
    if ((kargInfoV[ix].shortName == NULL) || (kargInfoV[ix].shortName[0] == 0))
      hasShortName = KFALSE;
    
    if (usageStringPrinted == KFALSE)
    {
      printf("Usage: %s  ", kargsProgName);
      usageStringPrinted = KTRUE;
    }
    else
      printf("%s", spaces);

    if (kargInfoV[ix].sort != KaReq)
      printf("[");

    if ((hasLongName == KTRUE) && (hasShortName == KTRUE))
      printf("%s/%s ", kargInfoV[ix].longName, kargInfoV[ix].shortName);
    else
      printf("%s ", hasLongName? kargInfoV[ix].longName : kargInfoV[ix].shortName);
    
    if (kargInfoV[ix].type == KaBool)
      printf("(%s)", kargInfoV[ix].description);
    else
      printf("<%s>", kargInfoV[ix].description);

    if (kargInfoV[ix].sort != KaReq)
      printf("]");

    printf("\n");

    ++ix;
  }
}



// -----------------------------------------------------------------------------
//
// maxCharsInLongName -
//
static int maxCharsInLongName(void)
{
  int maxLen = 0;
  int ix     = 0;

  while (kargInfoV[ix].type != KaEnd)
  {
    int len = strlen(kargInfoV[ix].longName);

    if (len > maxLen)
      maxLen = len;

    ++ix;
  }

  return maxLen;
}



// -----------------------------------------------------------------------------
//
// maxCharsInEnvVar -
//
static int maxCharsInEnvVar(void)
{
  int maxLen = 0;
  int ix     = 0;
  
  while (kargInfoV[ix].type != KaEnd)
  {
    int len = strlen(kargInfoV[ix].envVar);

    if (len > maxLen)
      maxLen = len;

    ++ix;
  }

  return maxLen;
}



// -----------------------------------------------------------------------------
//
// typeName - 
//
static char* typeName(KargType type)
{
  switch (type)
  {
  case KaBool:        return "Bool";
  case KaFloat:       return "Float";
  case KaString:      return "String";
  case KaChar:        return "Char";
  case KaUChar:       return "UChar";
  case KaShort:       return "Short";
  case KaUShort:      return "UShort";
  case KaInt:         return "Int";
  case KaUInt:        return "UInt";
  case KaLong:        return "Long";
  case KaULong:       return "ULong";
  case KaEnd:         return "No Type";
  }

  return "No Type";
}



// -----------------------------------------------------------------------------
//
// sortName - 
//
static char* sortName(KargSort sort)
{
  switch (sort)
  {
  case KaOpt: return "Optional";
  case KaReq: return "Required";
  case KaHid: return "Hidden";
  }

  return "No Sort";
}



// -----------------------------------------------------------------------------
//
// fromName - 
//
static char* fromName(KargValueFrom from)
{
  switch (from)
  {
  case KavfDefaultValue: return "Default Value";
  case KavfConfigFile:   return "Config File";
  case KavfEnvVar:       return "Env Var";
  case KavfShortOption:  return "Short Option";
  case KavfLongOption:   return "Long Option";
  }

  return "No From";
}



// -----------------------------------------------------------------------------
//
// kargsExtUsage -
//
// Usage: kargsTest  [--usage/-u (usage)]
//
// Long-Name Short-Name EnvVar  Type  Sort  From   Value  Def-Val  Min-Val  Max-Val  Is-Builtin Is-Disabled
//
void kargsExtUsage(void)
{
  char spaces[128];
  int  len = 7 + strlen(kargsProgName) + 2;

  KBL_V(("In"));

  // Protect against ridiculously long program names
  if (len >= sizeof(spaces))
    len = 2;
      
  memset(spaces, ' ', len);
  spaces[len] = 0;

  int    ix            = 0;
  int    longNameChars = maxCharsInLongName();
  int    envVarChars   = maxCharsInEnvVar();

  while (kargInfoV[ix].type != KaEnd)
  {
    char  format[32];
    char* sort = sortName(kargInfoV[ix].sort);
    char* type = typeName(kargInfoV[ix].type);
    char* from = fromName(kargInfoV[ix].from);

    if ((kargInfoV[ix].sort == KaHid) || (kargInfoV[ix].disabled == KTRUE))
    {
      ++ix;
      continue;
    }
    
    // Long Name
    KBool  hasLongName = KTRUE;
    if ((kargInfoV[ix].longName == NULL) || (kargInfoV[ix].longName[0] == 0))
      hasLongName = KFALSE;

    snprintf(format, sizeof(format), "%%%ds", longNameChars + 2);
    printf(format, (hasLongName == KTRUE)? kargInfoV[ix].longName  : "-");

    // Short Name
    KBool  hasShortName  = KTRUE;
    if ((kargInfoV[ix].shortName == NULL) || (kargInfoV[ix].shortName[0] == 0))
      hasShortName = KFALSE;

    if (hasShortName == KTRUE)
      printf(" (%s)", kargInfoV[ix].shortName);
    else
      printf("     ");

    // Env Var
    snprintf(format, sizeof(format), " %%-%ds", envVarChars + 1);
    printf(format, kargInfoV[ix].envVar);

    // Sort
    printf(" %-10s", sort);

    // Type
    printf(" %-8s", type);

    // From
    printf(" %-14s ", from);

    printf("\n");

    ++ix;
  }
}

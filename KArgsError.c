// -----------------------------------------------------------------------------
//
// FILE                  KArgsError.c - KArgsError struct for error handling
//
// AUTHOR                Ken Zangelin
//
// Copyright 2024 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <stdio.h>                     // NULL
#include <stdlib.h>                    // calloc

#include "kbase/kBasicLog.h"           // KBL_*

#include "kargs/KArgsStatus.h"         // KArgsStatus
#include "kargs/KArgsError.h"          // Own interface



// -----------------------------------------------------------------------------
//
// errorListHead - 
//
static KArgsError* errorListHead = NULL;



// -----------------------------------------------------------------------------
//
// kargsErrorPush - push an error to the error stack
//
// This is an internal function. It should be used *only* by the internal macro 'KARGS_ERROR_PUSH'.
//
void kargsErrorPush
(
  const char*  fileName,
  int          lineNo,
  const char*  funcName,
  const char*  optionName,
  int          errNo,
  KArgsStatus  status,
  const char*  description
)
{
  KArgsError* eP = (KArgsError*) calloc(1, sizeof(KArgsError));

  if (eP == NULL)  // Out of memory ...
    KBL_RV(("Out Of Memory"));

  eP->fileName     = (char*) fileName;
  eP->lineNo       = lineNo;
  eP->funcName     = (char*) funcName;
  eP->optionName   = (char*) optionName;
  eP->errNo        = errNo;
  eP->status       = status;
  eP->description  = (char*) description;

  // First error item?
  if (errorListHead == NULL)
  {
    errorListHead = eP;
    errorListHead->next = NULL;
  }
  else
  {
    //
    // Error item are inserted in the beginning of the list, so that
    // the lower level error items comes after in the list
    //
    eP->next      = errorListHead;
    errorListHead = eP;
  }

  if (kbVerbose == KTRUE)
    printf("%c:%s[%d]: %s: %s\n", 'E', eP->fileName, eP->lineNo, eP->funcName, eP->description);
}



// -----------------------------------------------------------------------------
//
// kargsErrorGet -
//
KArgsError* kargsErrorGet(void)
{
  return errorListHead;
}
